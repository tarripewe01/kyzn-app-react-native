
export const fonts = {
  Montserrat: {
    300: 'Montserrat Light',
    400: 'Montserrat Regular',
    500: 'Montserrat Medium',
    600: 'Montserrat SemiBold',
    700: 'Montserrat Bold',
    800: 'Montserrat ExtraBold',
    900: 'Montserrat Light',
  },
  Rubik: {
    300: 'Rubik-Light',
    400: 'Rubik-Regular',
    500: 'Rubik-Medium',
    600: 'Rubik-SemiBold',
    700: 'Rubik-Bold',
    800: 'Rubik-ExtraBold',
    900: 'Rubik-Light',
  },
};
