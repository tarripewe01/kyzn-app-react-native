import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {fonts} from '../../../utils';

const SelectServices = ({onPress, icon, title}) => {
  return (
    <View style={{paddingHorizontal: 10, marginBottom: 20}}>
      <TouchableOpacity onPress={onPress}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          {icon}
          <Text
            style={{
              marginLeft: 10,
              fontFamily: fonts.Montserrat[700],
              fontSize: 14,
            }}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default SelectServices;

const styles = StyleSheet.create({});
