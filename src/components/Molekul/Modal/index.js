import React from 'react'
import { StyleSheet, Text, View, ImageBackground } from 'react-native'
import Modal from 'react-native-modal';

const ModalAll = () => {
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  return (
    <Modal isVisible={isModalVisible}>
            <ImageBackground
              style={{width: 350}}
              source={require('../../../assets/Images/images_modal.png')}>
              <View style={{alignItems: 'center', marginTop: 40}}>
                <ICSuccess />
              </View>

              <View style={{alignItems: 'center', marginTop: 15}}>
                <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 16}}>
                  Claim was Successful
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.Montserrat[500],
                    fontSize: 12,
                    textAlign: 'center',
                    marginTop: 10,
                  }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 16,
                }}>
                <Image
                  style={{height: 24, width: 24}}
                  source={require('../../assets/Images/image_xp.png')}
                />
                <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 12}}>
                  800
                </Text>
              </View>

              <TouchableOpacity>
                <View
                  style={{
                    backgroundColor: colors.black2,
                    width: 320,
                    height: 42,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginHorizontal: 20,
                    borderRadius: 5,
                    marginTop: 40,
                    marginBottom: 50,
                  }}>
                  <Text
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 14,
                      color: colors.white2,
                    }}>
                    Done
                  </Text>
                </View>
              </TouchableOpacity>
            </ImageBackground>
          </Modal>
  )
}

export default ModalAll

const styles = StyleSheet.create({})
