import React from 'react';
import {StatusBar, View} from 'react-native';

const Bar = () => {
  return (
    <View>
      <StatusBar barStyle="dark-content" backgroundColor="white" />
    </View>
  );
};

export default Bar;
