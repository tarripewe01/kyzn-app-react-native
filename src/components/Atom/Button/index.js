import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const Button = ({title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={{paddingHorizontal: 16}}>
      <View style={styles.contain}>
        <Text style={styles.text}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  contain: {
    backgroundColor: colors.black1,
    width: '100%',
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {color: colors.white2, fontFamily: fonts.Montserrat[700], fontSize: 12},
});
