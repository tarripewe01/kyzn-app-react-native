import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {colors, fonts} from '../../../utils';

const ButtonBook = ({onPress}) => {
  return (
    <View>
      <View style={styles.containActionButton}>
        <View>
          <View style={styles.containDate}>
            <Text style={styles.textDate}>Today</Text>
            <View style={styles.dot}></View>
            <Text style={styles.textTime}>06:45</Text>
          </View>

          <View>
            <Text style={styles.textStatus}>3 Slots Available</Text>
          </View>
        </View>
        <TouchableOpacity onPress={onPress}>
          <View style={styles.containButton}>
            <Text style={styles.textButton}>BOOK</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ButtonBook;

const styles = StyleSheet.create({
  containActionButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  containDate: {flexDirection: 'row', alignItems: 'center'},
  textDate: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey10,
    marginRight: 5,
  },
  dot: {
    width: 5,
    height: 5,
    borderRadius: 50,
    backgroundColor: colors.grey10,
    borderWidth: 1,
    marginRight: 5,
  },
  textTime: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey10,
  },
  textStatus: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 14,
    marginTop: 5,
  },
  containButton: {
    backgroundColor: colors.blue1,
    width: 130,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  textButton: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 14,
    color: colors.white2,
  },
});
