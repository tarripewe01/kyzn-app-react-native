import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ICArrowBack} from '../../../assets';
import {Gap} from '../../../components';
import {colors, fonts} from '../../../utils';

const HeaderCourse = ({icon, title, onPress}) => {
  return (
    <View>
      <View style={styles.containHeader}>
        {/* <TouchableOpacity onPress={onPress}>{icon}</TouchableOpacity> */}
        <TouchableOpacity onPress={onPress}>
          <ICArrowBack />
        </TouchableOpacity>
        <Text style={styles.title}>{title}</Text>
      </View>
    </View>
  );
};

export default HeaderCourse;

const styles = StyleSheet.create({
  containHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 16,
    marginTop: 45,
  },
  title: {
    marginLeft: 10,
    fontFamily: fonts.Montserrat[700],
    fontSize: 16,
  },
});
