import React from 'react';
import { StyleSheet, View } from 'react-native';
import { colors } from '../../../utils';

const Divider = () => {
  return <View style={{borderWidth: 1, borderColor: colors.grey6}}></View>;
};

export default Divider;

const styles = StyleSheet.create({});
