import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import {colors, fonts} from '../../../utils';

const HeaderTitle = ({title, icon, onPress}) => {
  return (
    <View style={styles.containHeader}>
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity onPress={onPress}>
        {icon}
      </TouchableOpacity>
    </View>
  );
};

export default HeaderTitle;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white2,
    flex: 1,
  },
  containHeader: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 16,
    letterSpacing: 1,
  },
});
