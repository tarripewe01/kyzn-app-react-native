import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ICArrowBack, ICShare} from '../../../assets';
import {fonts} from '../../../utils';

const HeaderDetailClass = ({onPress, title, icon, onPressSearch}) => {
  return (
    <View>
      <View style={styles.containHeader}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={onPress}>
            <ICArrowBack />
          </TouchableOpacity>
          <Text style={styles.title}>{title}</Text>
        </View>
        <TouchableOpacity onPress={onPressSearch}>
          {icon}
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HeaderDetailClass;

const styles = StyleSheet.create({
  containHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginTop: 45,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    marginLeft: 10,
    fontFamily: fonts.Montserrat[700],
    fontSize: 16,
  },
});
