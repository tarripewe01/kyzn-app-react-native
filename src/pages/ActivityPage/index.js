import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Platform, ScrollView, StyleSheet, View} from 'react-native';
import {ICHistory} from '../../assets';
import {Bar, Button, Divider, Gap, HeaderTitle} from '../../components';
import {colors} from '../../utils';
import CardTitle from '../HomePage/components/CardTitle';
import CardActivity from './components/CardActivity';
import OrderList from './components/OrderList';

const ActivityPage = () => {
  const navigation = useNavigation();
  return (
    <>
      <Bar />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.page}>
          <View style={styles.header}>
            <HeaderTitle
              onPress={() => navigation.navigate('Payment History')}
              title="Activity"
              icon={<ICHistory />}
            />
          </View>

          <Gap height={24} />
          <CardActivity
            category="FACILITIES"
            course="Basketball"
            status="CONFIRMED"
            type="CONFIRMED"
            imgBg="activity"
          />
          <CardActivity
            category="KIDS"
            course="Boxing for Kids"
            status="WAITING LIST"
            type="WAITING LIST"
            imgBg="activity"
          />
          <CardActivity
            category="ADULTS"
            course="Yoga for Beginner"
            status="CONFIRMED"
            type="CONFIRMED"
            imgBg="activity"
          />
          <Gap height={20} />
          <Button
            onPress={() => navigation.navigate('All Activity')}
            title="VIEW ALL"
          />

          <Gap height={45} />
          <View>
            <CardTitle
              onPress={() => navigation.navigate('Payment History')}
              title="Ongoing Order & Payment"
            />
          </View>

          <Gap height={24} />
          <Divider />
          <Gap height={16} />
          <OrderList
            course="Yoga for Beginner"
            payment="BCA Virtual Account"
            date="2 Feb 2021"
            price="520.000"
            status="PENDING"
            type="PENDING"
          />

          <Gap height={24} />
          <Divider />
          <Gap height={16} />
          <OrderList
            course="3 Months Platinum Pass"
            payment="Credit Card"
            date="2 Feb 2021"
            price="499.000"
            status="COMPLETED"
            type="COMPLETED"
          />

          <Gap height={71} />
        </View>
      </ScrollView>
    </>
  );
};

export default ActivityPage;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white2,
    flex: 1,
  },
  header: {
    marginTop: Platform.OS === 'ios' ? 20 : -20,
  },
});
