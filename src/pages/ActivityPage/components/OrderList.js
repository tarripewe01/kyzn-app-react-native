import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {colors, fonts} from '../../../utils';

const OrderList = ({course, payment, price, status, date, type}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity onPress={() => navigation.navigate('Detail Activity')}>
      <View style={styles.page}>
        <View>
          <Text style={styles.title}>{course}</Text>
          <Text style={styles.subTitle}>{payment}</Text>
          <Text style={styles.date}>{date}</Text>
        </View>
        <View>
          <Text style={styles.title}>Rp {price}</Text>
          <Text style={styles.status(type)}>{status}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default OrderList;

const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {fontFamily: fonts.Montserrat[700], fontSize: 14},
  subTitle: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
    marginTop: 2,
  },
  status: type => ({
    fontFamily: fonts.Montserrat[700],
    fontSize: 11,
    color: type === 'PENDING' ? colors.yellow3 : colors.green2,
    textAlign: 'right',
    marginTop: 2,
  }),
  date: {
    marginTop: 11,
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
  },
});
