import React from 'react';
import {ImageBackground, Platform, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const imageBg = require('../../../assets/Images/image_cardActivity.png');
const imageBg2 = require('../../../assets/Images/images_detailactivity.png');

const CardActivity = ({category, course, status, type, imgBg, date}) => {
  return (
    <View style={styles.page}>
      {imgBg === 'activity' ? (
        <ImageBackground source={imageBg} style={styles.imageBg}>
          <View style={styles.containContent}>
            <View style={styles.content}>
              <View style={styles.containreminder}>
                <Text style={styles.textReminder}>Tommorrow</Text>
                <View style={styles.dot}></View>
                <Text style={styles.time}>10:00</Text>
              </View>
              <View style={styles.containCategory}>
                <Text style={styles.textCategory}>{category}</Text>
              </View>
            </View>
            <Text style={styles.course}>{course}</Text>
            <Text style={styles.status(type)}>{status}</Text>
          </View>
        </ImageBackground>
      ) : (
        <ImageBackground source={imageBg2} style={styles.imageBg}>
          <View style={styles.containContent}>
            <View style={styles.content}>
              <View style={styles.containreminder}>
                {date === 'date' ? (
                  <Text style={styles.textReminder}>1 Jan 2021</Text>
                ) : (
                  <Text style={styles.textReminder}>Tommorrow</Text>
                )}

                <View style={styles.dot}></View>
                <Text style={styles.time}>10:00</Text>
              </View>
              <View style={styles.containCategory}>
                <Text style={styles.textCategory}>{category}</Text>
              </View>
            </View>
            <Text style={styles.course}>{course}</Text>
            <Text style={styles.status(type)}>{status}</Text>
          </View>
        </ImageBackground>
      )}
    </View>
  );
};

export default CardActivity;

const styles = StyleSheet.create({
  page: {marginTop: 16, paddingHorizontal: 16},
  imageBg: {
    width: '100%',
    height: Platform.OS === 'ios' ? hp('11%') : hp('12%'),
  },
  containContent: {width: '100%', paddingTop: 16, paddingLeft: 16},
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 16,
  },
  containreminder: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textReminder: {
    marginRight: 4,
    fontFamily: fonts.Montserrat[600],
    fontSize: 10,
    color: colors.grey3,
  },
  dot: {
    height: 5,
    width: 5,
    borderRadius: 50,
    backgroundColor: colors.grey3,
    marginRight: 4,
    alignItems: 'center',
  },
  time: {
    marginRight: 4,
    fontFamily: fonts.Montserrat[600],
    fontSize: 10,
    color: colors.grey3,
  },
  containCategory: {
    width: 70,
    height: 17,
    backgroundColor: 'black',
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  textCategory: {
    color: 'white',
    fontFamily: fonts.Montserrat[700],
    fontSize: 8,
  },
  course: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    marginTop: 3,
  },
  status: type => ({
    fontFamily: fonts.Montserrat[700],
    fontSize: 8,
    color: type === 'CONFIRMED' ? colors.green2 : colors.yellow2,
    marginTop: 3,
  }),
});
