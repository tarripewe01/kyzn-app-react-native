import React from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {colors, fonts} from '../../../utils';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const imgStar = require('../../../assets/Images/image_star.png');

const CardDetailPage = ({title, price, image, type, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.page}>
      <Image style={styles.imgCourse} source={image} />
      <View style={styles.content}>
        <Text numberOfLines={1} style={styles.title}>
          {title}
        </Text>

        <View style={styles.price(type)}>
          <Text style={styles.textprice}>Rp {price}</Text>
          <View style={styles.containRating}>
            <Image style={styles.imgStar} source={imgStar} />
            <Text style={styles.rating}>4.7</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CardDetailPage;

const styles = StyleSheet.create({
  page: {
    borderWidth: 1,
    borderColor: colors.grey4,
    width: wp('43%'),
    height: Platform.OS === 'android' ? 250 : 230,
    marginRight: -10,
    marginLeft: 20,
  },
  imgCourse: {width: wp('43%'), height: 157},
  content: {paddingHorizontal: 10, paddingVertical: 12},
  title: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 13,
    marginBottom: 10,
  },
  price: type => ({
    justifyContent: 'space-between',
    flexDirection: type === '1' ? 'column' : 'row',
  }),
  textprice: {fontFamily: fonts.Montserrat[700], fontSize: 13},
  containRating: {flexDirection: 'row', alignItems: 'center'},
  imgStar: {height: 12, width: 12, marginRight: 5},
  rating: {fontFamily: fonts.Montserrat[700], fontSize: 13},
});
