import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Gap } from '../../../components'

const SlotPage = () => {
  return (
    <View style={{marginTop: 200}}>
      <Gap height={500}/>
      <Text>06:00</Text>
      <Text>Full</Text>
    </View>
  )
}

export default SlotPage

const styles = StyleSheet.create({})
