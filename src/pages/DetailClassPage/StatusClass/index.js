import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const StatusClass = (time, status) => {
  return (
    <View style={styles.page}>
      <Text style={styles.textTime}>06:00</Text>
      <Text style={styles.textStatus}>Full</Text>
    </View>
  );
};

export default StatusClass;

const styles = StyleSheet.create({
  page: {
    borderWidth: 1,
    width: 63,
    height: 49,
    borderColor: colors.black1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
    backgroundColor: colors.black1,
  },
  textTime: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 14,
    color: colors.white2,
  },
  textStatus: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.white2,
  },
});
