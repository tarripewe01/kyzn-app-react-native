import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import CalendarStrip from 'react-native-calendar-strip';
import {ICArrowNext, ICShare} from '../../assets';
import {Bar, Divider, Gap} from '../../components';
import ButtonBook from '../../components/Atom/ButtonBook';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {colors, fonts} from '../../utils';
import CardDetailPage from './CardDetailPage';
import StatusClass from './StatusClass';

const imgCourse = require('../../assets/Dummy/dummy_course1.png');
imgXp = require('../../assets/Images/image_xp.png');

const Tab = createMaterialTopTabNavigator();

const DetailClassPage = () => {
  const navigation = useNavigation();
  return (
    <ScrollView>
      <View style={styles.page}>
        <Bar />
        <View style={{marginTop: 20}}>
          <HeaderDetailClass
            title="Back"
            onPress={() => navigation.goBack('Wellness')}
            icon={<ICShare />}
          />
        </View>

        <Gap height={15} />
        <Image style={styles.imgCourse} source={imgCourse} />
        <Gap height={22} />
        <View style={styles.contain}>
          <Text style={styles.textCourse}>Yoga for Beginner</Text>
          <View style={styles.containPrice}>
            <Text style={styles.textPrice}>Rp 299.000</Text>
            <View style={styles.containReward}>
              <Image style={styles.imgXp} source={imgXp} />
              <Text style={styles.textXp}>800</Text>
            </View>
          </View>
        </View>

        <Gap height={16} />
        <Divider />
        <View style={styles.containLocation}>
          <View style={styles.containText}>
            <Text style={styles.title}>Kyzn Location</Text>
            <TouchableOpacity style={styles.containMap}>
              <Text style={styles.textMap}>See Map</Text>
              <ICArrowNext />
            </TouchableOpacity>
          </View>
          <View style={styles.containAddress}>
            <Text style={styles.textAddress}>
              Menara Kuningan - Jln Rasuna Sahid Ja...
            </Text>
            <Text style={styles.textDetail}>
              3th Floor - at basketball courts
            </Text>
          </View>
        </View>
        <Divider />

        <Gap height={24} />
        <View style={styles.containMember}>
          <Text style={styles.textMember}>Active membership is applied.</Text>
        </View>

        <CalendarStrip
          iconContainer={{flex: 0.1}}
          style={{height: 100, paddingTop: 10, paddingBottom: 10}}
        />

        <Gap height={32} />
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={{flexDirection: 'row'}}>
            <StatusClass />
            <StatusClass />
            <StatusClass />
            <StatusClass />
            <StatusClass />
          </View>
        </ScrollView>

        <View style={{marginTop: 32, paddingHorizontal: 20}}>
          <Text
            style={{
              fontFamily: fonts.Montserrat[500],
              fontSize: 12,
              lineHeight: 20,
              textAlign: 'justify',
            }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam...
          </Text>
          <TouchableOpacity
            style={{
              width: 96,
              height: 24,
              backgroundColor: colors.blue3,
              paddingHorizontal: 5,
              marginTop: 16,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 5,
            }}>
            <Text style={{fontFamily: fonts.Montserrat[500], fontSize: 12}}>
              Show More
            </Text>
          </TouchableOpacity>
        </View>

        <Gap height={32} />
        <View style={styles.containRekomend}>
          <Text style={styles.textRekomend}>Wellness you might like</Text>
        </View>
        <View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <CardDetailPage
              image={require('../../assets/Dummy/dummy_course1.png')}
              title="Yoga for Beginner"
              price="299.000"
              type=""
              onPress={() => navigation.navigate('Detail Class')}
            />
            <CardDetailPage
              image={require('../../assets/Dummy/dummy_course3.png')}
              title="Yoga Advance"
              price="199.000"
              type=""
            />
            <CardDetailPage
              image={require('../../assets/Dummy/dummy_course4.png')}
              title="Yoga for Kids"
              price="199.000"
              type=""
            />
          </ScrollView>
        </View>
        <Gap height={27} />
        <Divider />
        <ButtonBook onPress={() => navigation.navigate('Payment')} />

        <Gap height={15} />
      </View>
    </ScrollView>
  );
};

export default DetailClassPage;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
  imgCourse: {width: '100%', height: 300},
  contain: {paddingHorizontal: 20},
  textCourse: {fontFamily: fonts.Montserrat[700], fontSize: 16},
  containPrice: {flexDirection: 'row', marginTop: 5},
  textPrice: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 13,
    color: colors.blue1,
    marginRight: 5,
  },
  containReward: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgXp: {width: 14, height: 14},
  textXp: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 11,
    marginLeft: 3,
  },
  containLocation: {paddingHorizontal: 16, marginTop: 12},
  containText: {flexDirection: 'row', justifyContent: 'space-between'},
  title: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
  },
  containMap: {flexDirection: 'row', alignItems: 'center'},
  textMap: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    marginRight: 3,
  },
  containAddress: {marginTop: 10, marginBottom: 12},
  textAddress: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 13,
  },
  textDetail: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
    marginTop: 3,
  },
  containMember: {
    width: '100%',
    height: 40,
    backgroundColor: colors.purple3,
    paddingVertical: 9,
    alignItems: 'center',
  },
  textMember: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 13,
    color: colors.purple4,
    justifyContent: 'center',
  },
  containRekomend: {paddingHorizontal: 20, marginBottom: 16},
  textRekomend: {fontFamily: fonts.Montserrat[700], fontSize: 15},
});
