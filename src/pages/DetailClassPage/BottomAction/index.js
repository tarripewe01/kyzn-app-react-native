import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import {
  ImageBackground, StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Modal from 'react-native-modal';
import { ICSuccess } from '../../../assets';
import { colors, fonts } from '../../../utils';

const BottomAction = ({type, status, action, sport}) => {
  const navigation = useNavigation();
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  return (
    <View style={styles.containAction}>
      <View style={{flex: 1}}>
        <Modal isVisible={isModalVisible}>
          <ImageBackground
            style={{width: 350}}
            source={require('../../../assets/Images/images_modal.png')}>
            <View style={{alignItems: 'center', marginTop: 40}}>
              <ICSuccess />
            </View>

            <View style={{alignItems: 'center', marginTop: 15}}>
              <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 16}}>
                Booking was Successful.
              </Text>
              <Text
                style={{
                  fontFamily: fonts.Montserrat[500],
                  fontSize: 12,
                  textAlign: 'center',
                  marginTop: 10,
                }}>
                Yay! your booking was successful with booking ID KY2324346
              </Text>
            </View>

            <TouchableOpacity onPress={() => navigation.navigate('Detail Order')}>
              <View
                style={{
                  backgroundColor: colors.black2,
                  width: 320,
                  height: 42,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginHorizontal: 20,
                  borderRadius: 5,
                  marginTop: 40,
                  marginBottom: 10,
                }}>
                <Text
                  style={{
                    fontFamily: fonts.Montserrat[600],
                    fontSize: 14,
                    color: colors.white2,
                  }}>
                  View Details
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('MainApp')}>
              <View
                style={{
                  width: 320,
                  height: 42,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginHorizontal: 20,
                  borderRadius: 5,
                  marginBottom: 20,
                }}>
                <Text
                  style={{
                    fontFamily: fonts.Montserrat[600],
                    fontSize: 14,
                  }}>
                  Done
                </Text>
              </View>
            </TouchableOpacity>
          </ImageBackground>
        </Modal>
      </View>
      {/* <Modal
        animationType="slide"
        statusBarTranslucent={true}
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ICSuccess />
            <Text style={styles.modalText}>Booking was Successful.</Text>
            <Text style={styles.modalText1}>
              Yay! your booking was successful with booking ID KY2324346
            </Text>

            <Pressable
              onPress={() => navigation.navigate('MainApp')}
              style={[styles.button, styles.buttonClose]}>
              <Text style={styles.textStyle}>View Details</Text>
            </Pressable>

            <Pressable onPress={() => navigation.navigate('MainApp')}>
              <Text style={styles.textStyle1}>Done</Text>
            </Pressable>
          </View>
        </View>
      </Modal> */}
      <View style={styles.containActionButton}>
        <View>
          <View style={styles.containDate}>
            <Text style={styles.textDate}>Grand Total</Text>
          </View>

          <View>
            <Text style={styles.textStatus}>{status}</Text>
          </View>
        </View>
        <TouchableOpacity onPress={toggleModal}>
          <View style={styles.containButton}>
            <Text style={styles.textButton}>{action}</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BottomAction;

const styles = StyleSheet.create({
  containAction: {paddingHorizontal: 20, paddingVertical: 20},
  containActionButton: {flexDirection: 'row', justifyContent: 'space-between'},
  containDate: {flexDirection: 'row', alignItems: 'center'},
  textDate: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey10,
    marginRight: 5,
  },
  dot: {
    width: 5,
    height: 5,
    borderRadius: 50,
    backgroundColor: colors.grey10,
    borderWidth: 1,
    marginRight: 5,
  },
  textTime: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey10,
  },
  textStatus: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 14,
    marginTop: 5,
  },
  containButton: {
    backgroundColor: colors.blue1,
    width: 130,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  textButton: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 14,
    color: colors.white2,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 5,
    padding: 10,
  },
  buttonClose: {
    backgroundColor: colors.black1,
    width: 300,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: fonts.Montserrat[600],
    fontSize: 14,
  },
  textStyle1: {marginTop: 15, fontFamily: fonts.Montserrat[600], fontSize: 14},
  modalText: {
    marginBottom: 15,
    marginTop: 20,
    textAlign: 'center',
    fontFamily: fonts.Montserrat[700],
    fontSize: 16,
  },
  modalText1: {
    marginBottom: 15,
    textAlign: 'center',
    fontFamily: fonts.Montserrat[500],
    fontSize: 13,
  },
  contentWrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
