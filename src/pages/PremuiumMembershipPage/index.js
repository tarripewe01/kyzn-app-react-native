import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {colors, fonts} from '../../utils';
import {ICCheck} from '../../assets';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {BackgroundImage} from 'react-native-elements/dist/config';
import {Gap} from '../../components';
import CardPremiu from './CardPremiu';

const PremiumMembership = () => {
  const navigation = useNavigation();

  return (
    <View style={{flex: 1, backgroundColor: colors.white2}}>
      <ScrollView>
        <HeaderDetailClass
          title="Premium Memberships"
          onPress={() => navigation.goBack('Membership')}
        />
        <Gap height={16} />
        <CardPremiu
          title="3 Months Platinum Pass"
          price="499.999"
          banner={require('../../assets/Images/image_banner_profile.png')}
          onPress={() => navigation.goBack('Membership')}
        />
        <Gap height={16} />
        <CardPremiu
          title="6 Months Platinum Pass"
          price="2.999.999"
          banner={require('../../assets/Images/image_banner_profile2.png')}
        />
        <Gap height={16} />
        <CardPremiu
          title="12 Months Platinum Pass"
          price="RP 4.999.999"
          banner={require('../../assets/Images/image_banner_profile3.png')}
        />
        <Gap height={16} />
      </ScrollView>
    </View>
  );
};

export default PremiumMembership;

const styles = StyleSheet.create({});
