import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {BackgroundImage} from 'react-native-elements/dist/config';
import {ICCheck} from '../../assets';
import {Gap} from '../../components';
import {colors, fonts} from '../../utils';

const CardPremiu = ({banner, title, price, onPress}) => {
  const Info = () => {
    return (
      <View style={{flexDirection: 'row'}}>
        <ICCheck />
        <Text
          style={{
            fontFamily: fonts.Montserrat[500],
            fontSize: 10,
            marginLeft: 5,
            marginBottom: 10,
          }}>
          Benefit lorem ipsum dolor sit amet
        </Text>
      </View>
    );
  };
  return (
    <View>
      <BackgroundImage
        style={{
          width: 350,
          height: 85,
          marginHorizontal: 20,
          paddingHorizontal: 16,
          paddingTop: 16,
        }}
        source={banner}>
        <Text
          style={{
            fontFamily: fonts.Montserrat[700],
            fontSize: 12,
            color: colors.white2,
          }}>
          {title}
        </Text>
        <Text
          style={{
            fontFamily: fonts.Montserrat[700],
            fontSize: 14,
            color: colors.white2,
            marginTop: 5,
          }}>
          Rp {price}
        </Text>
      </BackgroundImage>
      <View
        style={{
          borderWidth: 1,
          borderColor: colors.grey2,
          width: 350,
          marginHorizontal: 20,
          // borderRadius: 5,
          borderBottomEndRadius: 5,
          borderBottomStartRadius: 5,
          paddingHorizontal: 10,
          paddingTop: 16,
        }}>
        <Text
          style={{
            fontFamily: fonts.Montserrat[600],
            fontSize: 12,
            color: colors.grey9,
            marginBottom: 15,
          }}>
          What you get :
        </Text>
        <Info />
        <Info />
        <Info />
        <Info />
        <Info />

        <TouchableOpacity onPress={onPress}>
          <View
            style={{
              width: 320,
              height: 34,
              backgroundColor: colors.black1,
              borderRadius: 5,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 20,
            }}>
            <Text
              style={{
                fontFamily: fonts.Montserrat[700],
                fontSize: 14,
                color: colors.white2,
              }}>
              Buy
            </Text>
          </View>
        </TouchableOpacity>

        <Gap height={20} />
      </View>
    </View>
  );
};

export default CardPremiu;

const styles = StyleSheet.create({});
