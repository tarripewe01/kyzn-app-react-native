import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import {colors, fonts} from '../../../utils';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const imgXp = require('../../../assets/Images/image_xp.png');

const CardSport = ({img, title, price}) => {
  const navigation = useNavigation();
  return (
    <>
      <View style={styles.page}>
        <TouchableOpacity onPress={() => navigation.navigate('Detail Sport')}>
          <View style={styles.card}>
            <Image style={{width: wp('45%')}} source={img} />
            <View style={{paddingHorizontal: 10}}>
              <Text style={styles.title}>{title}</Text>
              <View style={styles.containcontent}>
                <Text style={styles.price}>{price}</Text>
                <View style={styles.containreward}>
                  <Image style={styles.imgXp} source={imgXp} />
                  <Text style={styles.xp}>800</Text>
                </View>
              </View>
              <Text style={styles.discprice}>Rp 299.000</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default CardSport;

const styles = StyleSheet.create({
  page: {paddingHorizontal: 20, marginRight: -30},
  card: {
    width: wp('45%'),
    height: Platform.OS === 'android' ? hp('22%') : hp('19%'),
    borderWidth: 1,
    borderColor: colors.grey6,
  },
  title: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    marginTop: 12,
  },
  containcontent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
  },
  price: {fontFamily: fonts.Montserrat[700], fontSize: 11},
  containreward: {flexDirection: 'row', alignItems: 'center'},
  imgXp: {width: 12, height: 12},
  xp: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 11,
    marginLeft: 5,
  },
  discprice: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 10,
    marginTop: 3,
    color: colors.red2,
    textDecorationLine: 'line-through',
  },
});
