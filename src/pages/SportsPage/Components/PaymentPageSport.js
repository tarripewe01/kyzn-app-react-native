import React from 'react';
import {
  ImageBackground,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TextInput} from 'react-native-paper';
import {ICArrowNext, ICCreditCard, ICDiscount, ICVisa} from '../../../assets';
import {Divider, Gap} from '../../../components';
import HeaderCourse from '../../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../../utils';
import BottomAction from '../../DetailClassPage/BottomAction';

const imageBg = require('../../../assets/Images/image_bannerdetail.png');

const PaymentPageSport = () => {
  return (
    <View>
      <StatusBar
          barStyle="dark-content"
          translucent={true}
          backgroundColor="transparent"
        />
        <View>
          <ImageBackground style={styles.ImageBackground} source={imageBg}>
            <View style={{marginTop: -5}}>
              <HeaderCourse title="Payment" />
            </View>
          </ImageBackground>

          <Gap height={225} />
          <Divider />
          <BottomAction status="Rp 299.000" action="CHECKOUT" type="" sport='membership' />
        </View>
         
    </View>
  )
}

export default PaymentPageSport

const styles = StyleSheet.create({})
