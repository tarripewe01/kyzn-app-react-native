import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet, View} from 'react-native';
import {Bar, Gap} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors} from '../../utils';
import CardSport from './Components/CardSport';

const SportsPage = () => {
  const navigation = useNavigation();
  return (
    <>
      <Bar />
      <View style={styles.page}>
        <View style={{marginTop: 20}}>
          <HeaderCourse
            title="Group Class"
            onPress={() => navigation.navigate('MainApp')}
          />
        </View>

        <Gap height={31} />
        <View style={{flexDirection: 'row'}}>
          <CardSport
            img={require('../../assets/Dummy/dummy_sport1.png')}
            title="PULSE Basketball"
            price="FREE"
          />
          <CardSport
            img={require('../../assets/Dummy/dummy_sport2.png')}
            title="Social Basketball"
            price="Rp 150.000"
          />
        </View>

        <Gap height={20} />
        <View style={{flexDirection: 'row'}}>
          <CardSport
            img={require('../../assets/Dummy/dummy_sport3.png')}
            title="Skills Lab"
            price="FREE"
          />
        </View>
      </View>
    </>
  );
};

export default SportsPage;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
});
