import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Picker,
} from 'react-native';
import {TextInput} from 'react-native-paper';
import {ICCamera} from '../../assets';
import {Bar, Gap} from '../../components';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {colors, fonts} from '../../utils';

const EditAccountPage = () => {
  const navigation = useNavigation();

  const [name, setName] = useState('Andri Lesmana');
  const [email, setEmail] = useState('AndriLesmana@gmail.com');
  const [number, setNumber] = useState('08122353457');
  const [emergency, setEmergency] = useState('08122353457');
  const [gender, setGender] = useState('Male');
  const [birthday, setBirthday] = useState('23/08/2008');
  const [nation, setNation] = useState('Indonesia');

  return (
    <View style={styles.page}>
      <Bar />
      <View style={{marginTop: 20}}>
        <HeaderDetailClass
          title="Edit Profile"
          onPress={() => navigation.goBack('Account')}
        />
      </View>

      <View style={styles.containCamera}>
        <View style={styles.roundCamera}>
          <TouchableOpacity>
            <View style={styles.iconCamera}>
              <ICCamera />
            </View>
          </TouchableOpacity>
        </View>
      </View>

      <Gap height={40} />
      <View style={styles.padding}>
        <TextInput
          mode="outlined"
          label="Name"
          value={name}
          onChangeText={text => setName(text)}
          theme={{
            colors: {
              primary: colors.grey7,
              underlineColor: 'transparent',
            },
            fonts: {},
          }}
          style={styles.inputForm}
        />
      </View>

      <Gap height={16} />
      <View style={styles.padding}>
        <TextInput
          mode="outlined"
          label="Email"
          value={email}
          onChangeText={text => setEmail(text)}
          theme={{
            colors: {
              primary: colors.grey7,
              underlineColor: 'transparent',
            },
          }}
          style={styles.inputForm}
        />
      </View>

      <Gap height={16} />
      <View style={styles.padding}>
        <TextInput
          mode="outlined"
          label="Phone Number"
          value={number}
          onChangeText={text => setNumber(text)}
          theme={{
            colors: {
              primary: colors.grey7,
              underlineColor: 'transparent',
            },
          }}
          style={styles.inputForm}
        />
      </View>

      <Gap height={16} />
      <View style={styles.padding}>
        <TextInput
          mode="outlined"
          label="Emergency Number"
          value={emergency}
          onChangeText={text => setEmergency(text)}
          theme={{
            colors: {
              primary: colors.grey7,
              underlineColor: 'transparent',
            },
          }}
          style={styles.inputForm}
        />
      </View>

      <Gap mode="dialog" height={16} />
      <View
        style={{
          borderWidth: 1,
          width: 350,
          marginHorizontal: 20,
          marginTop: 10,
          borderRadius: 2,
          borderColor: colors.grey3,
          backgroundColor: colors.white1,
          height: 50,
        }}>
        <Picker>
          <Picker.Item
            style={{
              fontFamily: fonts.Montserrat[600],
              fontSize: 12,
              color: colors.grey3,
            }}
            label="Male"
            value="Male"
          />
          <Picker.Item
            style={{
              fontFamily: fonts.Montserrat[600],
              fontSize: 12,
              color: colors.grey3,
            }}
            label="Female"
            value="Female"
          />
        </Picker>
      </View>

      <Gap height={16} />
      <View style={styles.padding}>
        <TextInput
          mode="outlined"
          label="Birthday"
          value={birthday}
          onChangeText={text => setBirthday(text)}
          theme={{
            colors: {
              primary: colors.grey7,
              underlineColor: 'transparent',
            },
          }}
          style={styles.inputForm}
        />
      </View>

      <Gap height={16} />
      <View
        style={{
          borderWidth: 1,
          width: 350,
          marginHorizontal: 20,
          marginTop: 10,
          borderRadius: 2,
          borderColor: colors.grey3,
          backgroundColor: colors.white1,
          height: 50,
        }}>
        <Picker mode="dialog" style={{}}>
          <Picker.Item
            style={{
              fontFamily: fonts.Montserrat[600],
              fontSize: 12,
              color: colors.grey3,
            }}
            label="Indonesian"
            value="Indonesian"
          />
          <Picker.Item
            style={{
              fontFamily: fonts.Montserrat[600],
              fontSize: 12,
              color: colors.grey3,
            }}
            label="Foreign"
            value="Foreign"
          />
        </Picker>
      </View>

      <Gap height={40} />
      <TouchableOpacity onPress={() => navigation.goBack('Account')}>
        <View style={styles.containButton}>
          <Text style={styles.textButton}>SAVE</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default EditAccountPage;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  containCamera: {alignItems: 'center', marginTop: 16},
  roundCamera: {
    backgroundColor: colors.grey4,
    width: 80,
    height: 80,
    borderRadius: 50,
    alignItems: 'center',
  },
  iconCamera: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 80,
    height: 80,
  },
  padding: {paddingHorizontal: 20},
  inputForm: {
    width: 350,
    backgroundColor: colors.white1,
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
    height: 50,
  },
  inputForm: {
    width: 350,
    backgroundColor: colors.white1,
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
    height: 50,
  },
  inputForm: {
    width: 350,
    backgroundColor: colors.white1,
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
    height: 50,
  },
  inputForm: {
    width: 350,
    backgroundColor: colors.white1,
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
    height: 50,
  },
  container: {
    flex: 1,
    paddingTop: 10,
    alignItems: 'center',
  },
  containButton: {
    width: 350,
    height: 42,
    backgroundColor: colors.blue1,
    marginHorizontal: 20,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.white2,
  },
});
