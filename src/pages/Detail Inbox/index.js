import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {ICSuccess} from '../../assets';
import {Bar, Gap} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../utils';

const imgModal = require('../../assets/Images/images_modal.png');
const imgXp = require('../../assets/Images/image_xp.png');

const DetailInbox = () => {
  const navigation = useNavigation();

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <>
      <View style={styles.page}>
        <ScrollView>
          <Bar />
          <View style={{marginTop: 20}}>
            <HeaderCourse
              onPress={() => navigation.goBack('Inbox')}
              title="KYZN Promo"
            />
          </View>

          <Modal isVisible={isModalVisible}>
            <ImageBackground style={{width: 350}} source={imgModal}>
              <View style={styles.iconModalSuccess}>
                <ICSuccess />
              </View>

              <View style={styles.contentClaim}>
                <Text style={styles.textClaim}>Claim was Successful</Text>
                <Text style={styles.descClaim}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </Text>
              </View>

              <View style={styles.containReward}>
                <Image style={styles.imgXp} source={imgXp} />
                <Text style={styles.poin}>800</Text>
              </View>

              <TouchableOpacity onPress={() => navigation.navigate('MainApp')}>
                <View style={styles.containButton}>
                  <Text style={styles.textButton}>Done</Text>
                </View>
              </TouchableOpacity>
            </ImageBackground>
          </Modal>

          <Gap height={30} />
          <View style={{paddingHorizontal: 20, width: wp('98%')}}>
            <Text style={styles.title}>Description</Text>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                This is the detailed explanation on the benefits covered in this
                particular promo. Mauris tincidunt ipsum nec facilisis
                hendrerit. Nam gravida non urna et lobortis.
              </Text>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                Suspendisse sagittis elit sem. Proin dignissim efficitur
                commodo. Ut sodales at nibh quis iaculis. Aenean efficitur vel
                orci et mollis. Donec condimentum nulla nibh, sed imperdiet
                massa tincidunt vel.
              </Text>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                Aenean luctus sodales enim consequat tempor. Aenean elementum
                lacinia neque, ut malesuada justo.
              </Text>
            </View>
          </View>

          <Gap height={30} />
          <View style={{paddingHorizontal: 20, width: 395}}>
            <Text style={styles.title}>How to use</Text>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                1. This is the detailed explanation of How-to-use this
                particular promo. Lorem ipsum dolor sit amet, consectetur
                adipiscing elit. Mauris tincidunt ipsum nec facilisis hendrerit.
                Nam gravida non urna et lobortis.
              </Text>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                2. Suspendisse sagittis elit sem. Proin dignissim efficitur
                commodo.
              </Text>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                3. Ut sodales at nibh quis iaculis. Aenean efficitur vel orci et
                mollis. Donec condimentum nulla nibh, sed imperdiet massa
                tincidunt vel.
              </Text>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                4. Aenean luctus sodales enim consequat tempor. Aenean elementum
                lacinia neque, ut malesuada justo.
              </Text>
            </View>
          </View>

          <Gap height={30} />
          <View style={{paddingHorizontal: 20, width: 395}}>
            <Text style={styles.title}>Terms & conditions</Text>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                1. This is the detailed explanation on the terms & conditions on
                this particular promo. Lorem ipsum dolor sit amet, consectetur
                adipiscing elit. Mauris tincidunt ipsum nec facilisis hendrerit.
                Nam gravida non urna et lobortis.
              </Text>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                2. Suspendisse sagittis elit sem. Proin dignissim efficitur
                commodo.
              </Text>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                3. Ut sodales at nibh quis iaculis. Aenean efficitur vel orci et
                mollis. Donec condimentum nulla nibh, sed imperdiet massa
                tincidunt vel.
              </Text>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={styles.subtitle}>
                4. Aenean luctus sodales enim consequat tempor. Aenean elementum
                lacinia neque, ut malesuada justo.
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>

      <View style={{backgroundColor: colors.white2, height: hp('15%')}}>
        <TouchableOpacity onPress={toggleModal}>
          <View style={styles.containButtonClaim}>
            <Text style={styles.textButtonClaim}>Claim Now</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.goBack('Inbox')}>
          <View style={styles.buttonCancel}>
            <Text style={styles.textButtonCancel}>Cancel</Text>
          </View>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default DetailInbox;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  title: {fontFamily: fonts.Montserrat[600], fontSize: 14},
  subtitle: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 12,
    color: colors.grey12,
    lineHeight: 19.5,
    textAlign: 'justify',
  },
  iconModalSuccess: {alignItems: 'center', marginTop: 40},
  contentClaim: {alignItems: 'center', marginTop: 15},
  textClaim: {fontFamily: fonts.Montserrat[700], fontSize: 16},
  descClaim: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 12,
    textAlign: 'center',
    marginTop: 10,
  },
  containReward: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
  },
  imgXp: {height: 24, width: 24},
  poin: {fontFamily: fonts.Montserrat[700], fontSize: 12, marginLeft: 3},
  containButton: {
    backgroundColor: colors.black2,
    width: 320,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 20,
    borderRadius: 5,
    marginTop: 40,
    marginBottom: 50,
  },
  textButton: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 14,
    color: colors.white2,
  },
  containButtonClaim: {
    backgroundColor: colors.blue1,
    width: 350,
    height: 42,
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 15,
  },
  textButtonClaim: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 14,
    color: colors.white2,
  },
  buttonCancel: {
    width: 350,
    height: 42,
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  textButtonCancel: {fontFamily: fonts.Montserrat[600], fontSize: 14},
});
