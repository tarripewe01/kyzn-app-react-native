import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {Bar, Gap} from '../../components';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {colors, fonts} from '../../utils';
import CardActivity from '../ActivityPage/components/CardActivity';

const AllActivityPage = () => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: colors.white2}}>
      <Bar />
      <ScrollView>
        <View style={{marginTop: 20}}>
          <HeaderDetailClass
            onPress={() => navigation.goBack('Activity')}
            title="All Activity"
          />
        </View>

        <View style={styles.containStatus}>
          <Text style={styles.ongoing}>ONGOING ACTIVITY</Text>
        </View>

        <CardActivity
          course="Tennis Courts"
          category="Facilities"
          status="CONFIRMED"
          type="CONFIRMED"
        />
        <CardActivity
          course="Boxing for Kids"
          category="Kids"
          status="PENDING"
          type="PENDING"
        />
        <CardActivity
          course="Yoga for Beginner"
          category="Adults"
          status="CONFIRMED"
          type="CONFIRMED"
        />

        <View style={{paddingHorizontal: 20, marginTop: 32}}>
          <Text style={styles.ongoing}>HISTORY</Text>
        </View>

        <CardActivity
          course="Tennis Courts"
          category="Facilities"
          status="CONFIRMED"
          type="CONFIRMED"
          date="date"
        />
        <CardActivity
          course="Boxing for Kids"
          category="Kids"
          status="PENDING"
          type="PENDING"
          date="date"
        />
        <CardActivity
          course="Yoga for Beginner"
          category="Adults"
          status="CONFIRMED"
          type="CONFIRMED"
          date="date"
        />
        <CardActivity
          course="Boxing for Kids"
          category="Kids"
          status="PENDING"
          type="PENDING"
          date="date"
        />
        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

export default AllActivityPage;

const styles = StyleSheet.create({
  page: {marginTop: 16, paddingHorizontal: 16},
  containStatus: {paddingHorizontal: 20, marginTop: 32},
  ongoing: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 10,
    color: colors.black2,
  },
  imageBg: {width: '100%', height: 80},
  containContent: {width: '100%', paddingTop: 16, paddingLeft: 16},
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 16,
  },
  containreminder: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textReminder: {
    marginRight: 4,
    fontFamily: fonts.Montserrat[600],
    fontSize: 10,
    color: colors.grey3,
  },
  dot: {
    height: 5,
    width: 5,
    borderRadius: 50,
    backgroundColor: colors.grey3,
    marginRight: 4,
  },
  time: {
    marginRight: 4,
    fontFamily: fonts.Montserrat[600],
    fontSize: 10,
    color: colors.grey3,
  },
  containCategory: {
    width: 70,
    height: 17,
    backgroundColor: 'black',
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  textCategory: {
    color: 'white',
    fontFamily: fonts.Montserrat[700],
    fontSize: 8,
  },
  course: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 14,
    marginTop: 3,
  },
  status: type => ({
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    color: type === 'CONFIRMED' ? colors.green2 : colors.yellow2,
    marginTop: 3,
  }),
});
