import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
  SafeAreaView,
} from 'react-native';
import {ICArrowBack, ICEmail, ICGoogle, ICLock} from '../../assets';
import {Bar, Gap} from '../../components';
import {colors} from '../../utils/Colors';
import {fonts} from '../../utils/Fonts';

const {width, height} = Dimensions.get('window');

const RegisterPage = ({navigation}) => {
  return (
    <>
      <Bar />

      <View style={styles.page}>
        <Gap height={13} />

        <View style={styles.containHeader}>
          <TouchableOpacity onPress={() => navigation.goBack('Register')}>
            <ICArrowBack />
          </TouchableOpacity>
          <View style={styles.Header}>
            <Text style={styles.titleHeader}>Sign Up</Text>
          </View>
        </View>

        <Gap height={50} />

        <View style={styles.containInput}>
          <View style={styles.input}>
            <ICEmail />
            <TextInput placeholder="Email" style={styles.textInput} />
          </View>
        </View>

        <Gap height={24} />

        <View style={styles.containInput}>
          <View style={styles.input}>
            <ICLock />
            <TextInput
              secureTextEntry
              placeholder="Password"
              style={styles.textInput}
            />
          </View>
        </View>

        <Gap height={24} />

        <View style={styles.containInput}>
          <View style={styles.input}>
            <ICLock />
            <TextInput
              secureTextEntry
              placeholder="Confirm Password"
              style={styles.textInput}
            />
          </View>
        </View>

        <Gap height={24} />

        <TouchableOpacity
          onPress={() => navigation.goBack('Login')}
          style={styles.containButton}>
          <Text style={styles.textButton}>Register</Text>
        </TouchableOpacity>

        <Gap height={40} />

        <View style={styles.divider}></View>

        <Gap height={40} />

        <TouchableOpacity style={styles.containBtnGoogle}>
          <View style={styles.google}>
            <ICGoogle />
            <Text style={styles.textBtnGoogle}>Sign up via Google</Text>
          </View>
        </TouchableOpacity>

        <Gap height={40} />

        <View style={styles.link}>
          <Text>Already have account?</Text>
          <TouchableOpacity onPress={() => navigation.goBack('Login')}>
            <Text> Sign in here</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

export default RegisterPage;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white2,
    flex: 1,
    paddingHorizontal: 38,
    paddingTop: Platform.OS === 'ios' ? 30 : 0,
  },
  containHeader: {flexDirection: 'row', height: 50, alignItems: 'center'},
  Header: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    fontFamily: fonts.Montserrat[600],
  },

  titleHeader: {
    fontSize: height / 30,
    fontFamily: fonts.Montserrat[600],
    marginRight: 15,
  },
  containInput: {
    borderWidth: 1,
    borderColor: colors.grey1,
    width: width / 1.25,
    height: height / 15,
    borderRadius: 5,
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 12,
    height: Platform.OS === 'ios' ? 55 : 45,
  },
  textInput: {
    marginLeft: 6,
    fontSize: height / 60,
    fontFamily: fonts.Montserrat[500],
    flex: 1,
  },
  containButton: {
    backgroundColor: colors.blue1,
    height: height / 15,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    fontSize: height / 50,
    fontFamily: fonts.Montserrat[700],
    color: colors.white1,
  },
  divider: {borderWidth: 1, borderColor: colors.grey2},
  containBtnGoogle: {
    borderWidth: 1,
    borderColor: colors.black1,
    height: height / 15,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  google: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtnGoogle: {
    fontSize: height / 50,
    fontFamily: fonts.Montserrat[600],
    marginLeft: 4,
  },
  link: {flexDirection: 'row', justifyContent: 'center', marginBottom: 150},
});
