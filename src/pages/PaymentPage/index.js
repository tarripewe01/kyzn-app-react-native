import React from 'react';
import { useNavigation } from '@react-navigation/native';
import {
  ImageBackground,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TextInput} from 'react-native-paper';
import {ICArrowNext, ICCreditCard, ICDiscount, ICVisa} from '../../assets';
import {Divider, Gap} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../utils';
import BottomAction from '../DetailClassPage/BottomAction';

const imageBg = require('../../assets/Images/image_bannerdetail.png');

const PaymentPage = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.page}>
      <ScrollView>
        <StatusBar
          barStyle="dark-content"
          translucent={true}
          backgroundColor="transparent"
        />
        <View>
          <ImageBackground style={styles.ImageBackground} source={imageBg}>
            <View style={{marginTop: 15}}>
              <HeaderCourse title="Payment"  />
            </View>
          </ImageBackground>
          <Gap height={32} />
          <Text style={styles.textMethod}>Payment Method</Text>
          <Gap height={12} />
          <Divider />
          <Gap height={19} />
          <View style={styles.containOption}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <ICCreditCard />
              <Text style={styles.textOption}>CREDIT CARD</Text>
            </View>
            <TouchableOpacity>
              <ICArrowNext />
            </TouchableOpacity>
          </View>
          <Gap height={19} />
          <View style={styles.bigDivider}></View>
          <Text style={styles.textDetails}>Payment Details</Text>
          <View style={{marginTop: 27}}>
            <TextInput
              mode="outlined"
              theme={{
                colors: {primary: colors.blue1, underlineColor: 'transparent'},
              }}
              label="Cardholder Name"
              placeholder="Ex: Wenny"
              style={styles.formInput}
            />

            <Gap height={12} />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <TextInput
                mode="outlined"
                theme={{
                  colors: {
                    primary: colors.blue1,
                    underlineColor: 'transparent',
                  },
                }}
                label="Credit Card Number"
                placeholder="123456789"
                style={styles.formInput}
              />
              <View style={{marginLeft: -50}}>
                <ICVisa />
              </View>
            </View>

            <Gap height={12} />
            <View style={{flexDirection: 'row', marginRight: 20}}>
              <TextInput
                mode="outlined"
                theme={{
                  colors: {
                    primary: colors.blue1,
                    underlineColor: 'transparent',
                  },
                }}
                label="Expire Date"
                placeholder="mm/yy"
                right={<ICVisa />}
                style={styles.halfFormInput}
              />
              <TextInput
                mode="outlined"
                theme={{
                  colors: {
                    primary: colors.blue1,
                    underlineColor: 'transparent',
                  },
                }}
                label="CVV"
                placeholder=""
                right={<ICVisa />}
                style={styles.halfFormInput}
              />
            </View>

            <Gap height={32} />
            <View style={styles.bigDivider}></View>

            <Gap height={24} />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <TextInput
                mode="outlined"
                theme={{
                  colors: {
                    primary: colors.blue1,
                    underlineColor: 'transparent',
                  },
                }}
                label="Promo Code"
                placeholder=""
                style={styles.formInput}
              />
              <View style={{marginLeft: -40}}>
                <ICDiscount />
              </View>
            </View>
          </View>

          <Gap height={32} />
          <View style={styles.containTotal}>
            <View>
              <Text style={styles.text1}>Yoga for Beginner</Text>
              <Text style={styles.textStatus}>Adults</Text>
            </View>
            <View style={styles.containPrice}>
              <Text style={styles.text2}>Rp 299.000</Text>
            </View>
          </View>

          <View style={{marginTop: 13, paddingHorizontal: 30}}>
            <View style={styles.containDiscount}>
              <Text style={styles.text1}>Discount</Text>
              <Text>-</Text>
            </View>
          </View>

          <Gap height={32} />
          <View style={styles.containGrandTotal}>
            <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
              <Text style={styles.textTotal}>Total</Text>
              <Text style={styles.textTotalPrice}>Rp 299.000</Text>
            </View>
          </View>

          <Gap height={225} />
          <Divider />
          <BottomAction status="Rp 299.000" action="CHECKOUT" type="" />
        </View>
      </ScrollView>
    </View>
  );
};

export default PaymentPage;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
  ImageBackground: {width: '100%', height: 100},
  textMethod: {
    paddingLeft: 20,
    fontFamily: fonts.Montserrat[600],
    fontSize: 13,
  },
  containOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  textOption: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 13,
    marginLeft: 10,
  },
  bigDivider: {
    borderWidth: 1,
    borderColor: colors.grey4,
    height: 8,
    backgroundColor: colors.grey4,
  },
  textDetails: {
    paddingLeft: 20,
    fontFamily: fonts.Montserrat[600],
    fontSize: 13,
    marginTop: 24,
  },
  formInput: {width: '95%', height: 46, paddingLeft: 20, backgroundColor: colors.grey11},
  halfFormInput: {width: '50%', height: 46, paddingLeft: 20, backgroundColor: colors.grey11},
  containTotal: {
    width: 350,
    height: 57,
    backgroundColor: colors.grey11,
    marginHorizontal: 20,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text1: {fontFamily: fonts.Montserrat[600], fontSize: 13},
  textStatus: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey1,
    marginTop: 3,
  },
  text2: {fontFamily: fonts.Montserrat[600], fontSize: 14},
  containPrice: {alignItems: 'center', justifyContent: 'center'},
  containGrandTotal: {
    width: 350,
    height: 57,
    backgroundColor: colors.grey11,
    marginHorizontal: 20,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  containDiscount: {flexDirection: 'row', justifyContent: 'space-between'},
  textTotal: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 14,
    marginRight: 26,
  },
  textTotalPrice: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 14,
    color: colors.blue1,
  },
});
