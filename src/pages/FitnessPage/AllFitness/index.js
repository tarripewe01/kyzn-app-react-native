import React from 'react';
import {useNavigation} from '@react-navigation/native';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {Gap} from '../../../components';
import {ICArrowDwon} from '../../../assets';
import {colors, fonts} from '../../../utils';
import CardFitness from '../components/CardFitness';

const AllFitness = () => {
  return (
    <View style={styles.page}>
      <ScrollView>
        <Gap height={24} />
        <View style={styles.contentText}>
          <Text style={styles.title}>Showing all 20 classes</Text>
          <TouchableOpacity style={styles.contentAction}>
            <Text style={styles.sort}>Sort by</Text>
            <ICArrowDwon />
          </TouchableOpacity>
        </View>

        <Gap height={20} />
        <View style={{flexDirection: 'row', paddingHorizontal: 20}}>
          <CardFitness title="Cardio" price="199.000" />
          <CardFitness title="Cycling" price="299.000" />
        </View>

        <View
          style={{flexDirection: 'row', paddingHorizontal: 20, marginTop: 20}}>
          <CardFitness title="Bench Press" price="199.000" />
          <CardFitness title="Leg Press" price="299.000" />
        </View>

        <View
          style={{flexDirection: 'row', paddingHorizontal: 20, marginTop: 20}}>
          <CardFitness title="Bench Press" price="199.000" />
          <CardFitness title="Leg Press" price="299.000" />
        </View>

        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

export default AllFitness;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
  contentText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 22,
    paddingRight: 18,
  },
  title: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 12,
    color: colors.grey9,
  },
  contentAction: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  sort: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    marginRight: 5,
  },
});
