import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import {colors, fonts} from '../../../utils';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const imgcourse = require('../../../assets/Dummy/dummy_fitness.png');

const CardFitness = ({title, price}) => {
  const navigation = useNavigation();
  return (
    <>
      <TouchableOpacity onPress={() => navigation.navigate('Detail Fitness')}>
        <View style={styles.conatiner}>
          <View style={styles.card}>
            <Image style={styles.imgcourse} source={imgcourse} />
            <View style={styles.contain}>
              <Text style={styles.title}>{title}</Text>
              <Text style={styles.price}>Rp {price}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default CardFitness;

const styles = StyleSheet.create({
  conatiner: {marginRight: 10},
  card: {
    width: wp('45%'),
    height: Platform.OS === 'android' ? hp('30%') : hp('27%'),
    borderWidth: 1,
    borderColor: colors.grey4,
  },
  imgcourse: {width: wp('45%'), height: 158},
  contain: {marginTop: 12, paddingHorizontal: 12},
  title: {fontFamily: fonts.Montserrat[700], fontSize: 14},
  price: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    marginTop: 8,
  },
});
