import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { ICArrowBack } from '../../assets';
import { Bar } from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import { colors, fonts } from '../../utils';
import { SwimmingClass } from '../WellnessPage/Classes';
import AllFitness from './AllFitness';
import CardioFitness from './CardioFitness';
import FatLossFitness from './FatLossFitness';
import HiitFitness from './HiitFitness';

const Tab = createMaterialTopTabNavigator();

const FitnessPage = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.page}>
      <Bar />
      <View style={{marginTop: 20}}>
        <HeaderCourse
          icon={<ICArrowBack />}
          title="Fitness"
          onPress={() => navigation.navigate('MainApp')}
        />
      </View>

      <Tab.Navigator
        screenOptions={{
          tabBarLabelStyle: {fontSize: 12, fontFamily: fonts.Montserrat[700]},
          tabBarStyle: {width: '100%', paddingTop: 10},
          tabBarAllowFontScaling: true,
          tabBarLabelStyle: {textTransform: 'none'},
          tabBarIndicatorStyle: {backgroundColor: colors.black1},
          tabBarItemStyle: {width: 100, paddingHorizontal: -10},
          tabBarScrollEnabled: true,
        }}>
        <Tab.Screen name="All" component={AllFitness} />
        <Tab.Screen name="Cardio" component={CardioFitness} />
        <Tab.Screen name="Fat Loss" component={FatLossFitness} />
        <Tab.Screen name="HIIT" component={HiitFitness} />
        <Tab.Screen name="Swimming" component={SwimmingClass} />
      </Tab.Navigator>
    </View>
  )
}

export default FitnessPage

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
})
