import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {colors} from '../../utils/Colors';
import {ICSearch} from '../../assets';
import {fonts} from '../../utils';

const SearchPage = () => {
  return (
    <>
      <View style={styles.page}>
        <View style={styles.containInput}>
          <View style={styles.input}>
            <ICSearch />
            <TextInput placeholder="Search" style={styles.textInput} />
          </View>
        </View>
      </View>
    </>
  );
};

export default SearchPage;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white2,
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  containInput: {
    borderWidth: 1,
    borderColor: colors.grey1,
    width: 350,
    height: 42,
    borderRadius: 5,
    marginTop: 70,
  },
  input: {flexDirection: 'row', alignItems: 'center', paddingLeft: 12},
  textInput: {
    marginLeft: 6,
    fontSize: 14,
    fontFamily: fonts.Montserrat[500],
    flex: 1,
  },
});
