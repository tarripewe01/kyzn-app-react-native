import React, {useEffect, useRef, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {ICArrowNext} from '../../assets';
import Bar from '../../components/Atom/Bar';
import {colors, fonts} from '../../utils';
import Indicators from '../SplashPage/Indicators';
import {LoginPage} from '..';

const imgOnboarding = require('../../assets/Images/images_onboarding.jpg');
const imgDecor = require('../../assets/Images/images_decor.png');

const slides = [
  {
    key: 1,
    title: 'Train everything with KYZN!',
    subtitle: 'family account that could track your kids training progress.',
  },
  {
    key: 2,
    title: 'Kids training Program',
    subtitle: 'family account that could track your kids training progress.',
  },
  {
    key: 3,
    title: 'Food & Beverages',
    subtitle: 'family account that could track your kids training progress.',
  },
];

const OnboardingPage = ({navigation}) => {
  if (!slides || !slides.length) return null;

  const [currentSlideIndex, setCurrentSlideIndex] = useState(0);
  const [loading, setLoading] = useState(true);
  const [isFirstTimeLoad, setIsFirstTimeLoad] = useState(false);
  
  const flatListRef = useRef();

  const checkForFirstTimeLoaded = async () => {
    const result = await AsyncStorage.getItem('isFirstTimeOpen');
    if (result === null) setIsFirstTimeLoad(true);
    setLoading(false);
  };

  useEffect(() => {
    checkForFirstTimeLoaded();
  }, []);

  const onViewableItemsChanged = useRef(item => {
    const index = item.viewableItems[0].index;
    setCurrentSlideIndex(index);
  });

  const handleSkip = () => {
    flatListRef.current.scrollToEnd({animated: true});
  };

  const handleNext = () => {
    if (currentSlideIndex >= slides.length - 1) return;
    flatListRef.current.scrollToIndex({index: currentSlideIndex + 1});
  };

  const onDone = () => {
    setIsFirstTimeLoad(false);
    AsyncStorage.setItem('isFirstTimeOpen', 'no');
  };

  if (loading) return null;

  if (isFirstTimeLoad)
    return (
      <>
        <View style={styles.page}>
          <Bar />
          <View style={styles.contain}>
            <Image style={styles.imgOnboarding} source={imgOnboarding} />
          </View>
          <FlatList
            ref={flatListRef}
            horizontal={true}
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            data={slides}
            keyExtractor={item => item.key.toString()}
            onViewableItemsChanged={onViewableItemsChanged.current}
            renderItem={({item}) => (
              <>
                <View style={styles.containText}>
                  <Text style={styles.title}>{item.title}</Text>
                  <Text style={styles.subtitle}>{item.subtitle}</Text>
                </View>
              </>
            )}
          />
          <View>
            <ImageBackground style={styles.decor} source={imgDecor}>
              <View style={styles.containIndicator}>
                <Indicators
                  currentSlideIndex={currentSlideIndex}
                  indicatorCount={slides.length}
                />
              </View>
              <View style={{marginHorizontal: 15}}>
                {currentSlideIndex < slides.length - 1 && (
                  <Text
                    onPress={handleSkip}
                    style={[styles.button, styles.leftButton]}>
                    Skip
                  </Text>
                )}

                {currentSlideIndex < slides.length - 1 ? (
                  <TouchableWithoutFeedback onPress={handleNext}>
                    <View style={styles.rightButtonNext}>
                      <ICArrowNext />
                    </View>
                  </TouchableWithoutFeedback>
                ) : (
                  <TouchableWithoutFeedback onPress={onDone}>
                    <View style={styles.rightButtonNext}>
                      <ICArrowNext />
                    </View>
                  </TouchableWithoutFeedback>
                )}
              </View>
            </ImageBackground>
          </View>
        </View>
      </>
    );
  if (!isFirstTimeLoad)
    return (
      <LoginPage
        login={() => navigation.replace('MainApp')}
        register={() => navigation.navigate('Register')}
      />
    );
};

export default OnboardingPage;

const {width, height} = Dimensions.get('screen');
const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  contain: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 51,
  },
  imgOnboarding: {width: 230, height: 400},
  containText: {marginTop: 30, paddingHorizontal: 20, width, height},
  title: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 24,
    width: 284,
  },
  subtitle: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 12,
    width: 259,
    marginTop: 13,
  },
  decor: {width: 400, height: 300, marginTop: -80, marginBottom: -160},
  containIndicator: {
    position: 'absolute',
    width,
    marginTop: 35,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  indicator: {
    width: 10,
    height: 10,
    backgroundColor: colors.black2,
    borderRadius: 5,
    marginHorizontal: 5,
  },
  button: {fontFamily: fonts.Montserrat[700], fontSize: 12, letterSpacing: 1},
  leftButton: {position: 'absolute', left: 10, marginTop: 30},
  rightButton: {position: 'absolute', right: 10, marginTop: 30},
  rightButtonNext: {
    height: 40,
    width: 40,
    backgroundColor: colors.yellow1,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 10,
    marginTop: 20,
  },
});
