import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import ReactNativeCalendarStrip from 'react-native-calendar-strip';
import {ICArrowNext, ICShare} from '../../assets';
import {Divider, Gap} from '../../components';
import ButtonBook from '../../components/Atom/ButtonBook';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {colors, fonts} from '../../utils';
import CardDetailPage from '../DetailClassPage/CardDetailPage';
import StatusClass from '../DetailClassPage/StatusClass';

const imgTrainer = require('../../assets/Dummy/dummy_trainer1.png');
const imgStar = require('../../assets/Images/image_star.png');

const DetailTrainerPage = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.page}>
      <ScrollView>
        <StatusBar
          barStyle="dark-content"
          translucent={true}
          backgroundColor="transparent"
        />
        <View style={styles.bgcolor}>
          <View style={styles.marginHeader}>
            <HeaderDetailClass
              title=""
              onPress={() => navigation.navigate('Trainer')}
              icon={<ICShare />}
            />
          </View>
        </View>

        <View style={{alignItems: 'center'}}>
          <Image style={styles.imgTrainer} source={imgTrainer} />
          <View style={styles.containTitle}>
            <Text style={styles.name}>Khazan Jacob</Text>
            <Image style={styles.imgStar} source={imgStar} />
            <Text style={styles.rating}>4.7</Text>
          </View>
          <View style={styles.containdesc}>
            <Text style={styles.desc}>
              Pound Fit 1on1, Cardio, Pilates,Yoga and Functional Training 1on1
            </Text>
          </View>
        </View>

        <View style={styles.containTraining}>
          <Text style={styles.training}>Select Training</Text>
          <View style={styles.containInfo}>
            <Text style={styles.course}>Basketball</Text>
            <Text style={styles.price}>IDR 250.000</Text>
            <View style={styles.containTime}>
              <Text style={styles.time}>60 min session</Text>
              <View style={styles.dot}></View>
              <Text style={styles.person}>Max 3 persons</Text>
            </View>
          </View>
        </View>

        <Gap height={16} />
        <Divider />
        <View style={styles.containLocation}>
          <View style={styles.containText}>
            <Text style={styles.title}>Kyzn Location</Text>
            <TouchableOpacity style={styles.containMap}>
              <Text style={styles.textMap}>See Map</Text>
              <ICArrowNext />
            </TouchableOpacity>
          </View>
          <View style={styles.containAddress}>
            <Text style={styles.textAddress}>
              Menara Kuningan - Jln Rasuna Sahid Ja...
            </Text>
            <Text style={styles.textDetail}>
              3th Floor - at basketball courts
            </Text>
          </View>
        </View>
        <Divider />

        <Gap height={24} />
        <View style={styles.containMember}>
          <Text style={styles.textMember}>Active membership is applied.</Text>
        </View>

        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{}}>{/* <ICAccount /> */}</View>
          <ReactNativeCalendarStrip
            iconContainer={{flex: 0.2}}
            style={{height: 100, paddingTop: 10, paddingBottom: 10, width: 400}}
          />
        </View>

        {/* <Gap height={20} /> */}
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={{flexDirection: 'row'}}>
            <StatusClass />
            <StatusClass />
            <StatusClass />
            <StatusClass />
            <StatusClass />
          </View>
        </ScrollView>

        <View style={{marginTop: 32, paddingHorizontal: 20}}>
          <Text
            style={{
              fontFamily: fonts.Montserrat[500],
              fontSize: 12,
              lineHeight: 20,
              textAlign: 'justify',
            }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam...
          </Text>
          <TouchableOpacity
            style={{
              width: 96,
              height: 24,
              backgroundColor: colors.blue3,
              paddingHorizontal: 5,
              marginTop: 16,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 5,
            }}>
            <Text style={{fontFamily: fonts.Montserrat[500], fontSize: 12}}>
              Show More
            </Text>
          </TouchableOpacity>
        </View>

        <Gap height={32} />
        <View style={styles.containRekomend}>
          <Text style={styles.textRekomend}>
            Personal Trainer you might like
          </Text>
        </View>

        <View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <CardDetailPage
              image={require('../../assets/Dummy/dummy_trainer1.png')}
              title="Shameer Jainab"
              price="299.000"
              type="2"
              onPress={() => navigation.navigate('Detail Trainer')}
            />
            <CardDetailPage
              image={require('../../assets/Dummy/dummy_trainer2.png')}
              title="Khazan Jacob"
              price="199.000"
              type="2"
              onPress={() => navigation.navigate('Detail Trainer')}
            />
            <CardDetailPage
              image={require('../../assets/Dummy/dummy_trainer3.png')}
              title="Jason"
              price="199.000"
              type="2"
              onPress={() => navigation.navigate('Detail Trainer')}
            />
          </ScrollView>
        </View>
        <Gap height={27} />
        <Divider />
        <ButtonBook onPress={() => navigation.navigate('Payment')} />
        <Gap height={15} />
      </ScrollView>
    </View>
  );
};

export default DetailTrainerPage;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
  bgcolor: {backgroundColor: colors.grey11, height: 150},
  marginHeader: {marginTop: 20},
  imgTrainer: {
    width: 86,
    height: 86,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -40,
  },
  containTitle: {flexDirection: 'row', alignItems: 'center'},
  name: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 16,
    marginRight: 6,
  },
  imgStar: {width: 12, height: 12},
  rating: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 11,
    marginLeft: 5,
  },
  containdesc: {width: 255, marginTop: 10},
  desc: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 11,
    color: colors.grey8,
    textAlign: 'center',
  },
  containTraining: {paddingHorizontal: 20, marginTop: 32},
  training: {fontFamily: fonts.Montserrat[700], fontSize: 14},
  containInfo: {
    borderWidth: 1,
    height: Platform.OS === 'android' ? hp('12%') : hp('10%'),
    width: 350,
    borderRadius: 8,
    borderColor: colors.grey5,
    marginTop: 12,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  course: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey8,
  },
  price: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    marginTop: 3,
  },
  containTime: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  time: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 10,
    color: colors.grey3,
    marginRight: 5,
  },
  dot: {
    width: 5,
    height: 5,
    borderRadius: 30,
    backgroundColor: colors.grey3,
    marginRight: 5,
  },
  person: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 10,
    color: colors.grey3,
  },

  containLocation: {paddingHorizontal: 16, marginTop: 12},
  containText: {flexDirection: 'row', justifyContent: 'space-between'},
  title: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
  },
  containMap: {flexDirection: 'row', alignItems: 'center'},
  textMap: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    marginRight: 3,
  },
  containAddress: {marginTop: 10, marginBottom: 12},
  textAddress: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 13,
  },
  textDetail: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.grey3,
    marginTop: 3,
  },
  containMember: {
    width: '100%',
    height: 40,
    backgroundColor: colors.purple3,
    paddingVertical: 9,
    alignItems: 'center',
  },
  textMember: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 13,
    color: colors.purple4,
    justifyContent: 'center',
  },
  containRekomend: {paddingHorizontal: 20, marginBottom: 16},
  textRekomend: {fontFamily: fonts.Montserrat[700], fontSize: 15},
});
