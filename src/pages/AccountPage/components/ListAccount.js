import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ICArrowNext} from '../../../assets';
import {Divider} from '../../../components';
import {fonts} from '../../../utils';

const ListAccount = ({title, icon, onPress}) => {
  return (
    <>
      <Divider />
      <TouchableOpacity onPress={onPress}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 16,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {icon}
            <Text
              style={{
                fontFamily: fonts.Montserrat[600],
                fontSize: 12,
                marginLeft: 5,
              }}>
              {title}
            </Text>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity>
              <ICArrowNext />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default ListAccount;

const styles = StyleSheet.create({});
