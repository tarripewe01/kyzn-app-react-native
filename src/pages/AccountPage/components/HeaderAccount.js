import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  ImageBackground,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Avatar, ListItem} from 'react-native-elements';
import Modal from 'react-native-modal';
import {ProgressBar} from 'react-native-paper';
import {ICArrowDwon, ICPremium, ICProfile} from '../../../assets';
import {colors, fonts} from '../../../utils';

const imageBg = require('../../../assets/Images/image_hero_profile.png');
const imageBanner = require('../../../assets/Images/image_banner_profile.png');

const list = [
  {
    id: 1,
    name: 'Wenny',
    subtitle: 'As Parents',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
  },
  {
    id: 2,
    name: 'Indrayana',
    subtitle: '3 Sports Joined',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
  },
  {
    id: 3,
    name: 'Agnesia Montania',
    subtitle: 'As Parents',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
  },
];

const HeaderAccount = ({onPress, isModalVisible, toggleModal}) => {
  const navigation = useNavigation();
  // const [isModalVisible, setModalVisible] = useState(false);

  // const toggleModal = () => {
  //   setModalVisible(!isModalVisible);
  // };
  return (
    <>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <Modal isVisible={isModalVisible}>
        <ImageBackground
          style={{width: 350}}
          source={require('../../../assets/Images/images_modal.png')}>
          <View style={{marginTop: 40, paddingHorizontal: 20}}>
            <Text style={{fontFamily: fonts.Montserrat[600], fontSize: 12}}>
              Switch Accounts
            </Text>
          </View>

          <View>
            {list.map((l, i) => (
              <TouchableOpacity onPress={onPress}>
                <ListItem key={l.id} bottomDivider>
                  <Avatar rounded source={{uri: l.avatar_url}} />
                  <ListItem.Content>
                    <ListItem.Title>{l.name}</ListItem.Title>
                    <ListItem.Subtitle>{l.subtitle}</ListItem.Subtitle>
                  </ListItem.Content>

                  <ListItem.CheckBox
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                  />
                </ListItem>
              </TouchableOpacity>
            ))}
          </View>
        </ImageBackground>
      </Modal>

      <ImageBackground source={imageBg} style={styles.imageBg}>
        <View style={styles.containProfile}>
          <View style={styles.content}>
            <ICProfile />
            <View style={styles.containTitle}>
              <View style={styles.title}>
                <View style={styles.containName}>
                  <Text style={styles.textName}>Andri Lesmana</Text>
                  <TouchableOpacity style={styles.iconArrow}>
                    <ICArrowDwon />
                  </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={toggleModal}>
                  <Text style={styles.textSwitch}>Switch Account</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.icon}>
                <ICPremium />
              </View>
            </View>
          </View>
        </View>
        <View style={styles.containImage}>
          <TouchableOpacity onPress={() => navigation.navigate('Membership')}>
            <ImageBackground source={imageBanner} style={styles.imgBanner}>
              <View style={styles.containBannerTitle}>
                <Text style={styles.textBannerTitle}>
                  3 Months Platinum Pass
                </Text>
                <View style={styles.containStep}>
                  <Text style={styles.textStep}>4x LEFT</Text>
                </View>
              </View>
              <View style={styles.containInfo}>
                <Text style={styles.textDate}>
                  START 21 DEC 2020 - END 21 JAN 2021
                </Text>
                <View style={styles.containProgress}>
                  <ProgressBar progress={0.7} color={colors.white2} />
                </View>
              </View>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </>
  );
};

export default HeaderAccount;

const styles = StyleSheet.create({
  imageBg: {width: '100%', height: 156},
  containProfile: {marginTop: 50},
  content: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  containTitle: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
  title: {marginLeft: 15},
  containName: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textName: {fontFamily: fonts.Montserrat[700], fontSize: 14},
  iconArrow: {marginLeft: 5},
  textSwitch: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    marginTop: 5,
  },
  icon: {marginTop: 10},
  containImage: {justifyContent: 'center', alignItems: 'center'},
  imgBanner: {width: 350, height: 90, marginTop: 24},
  containBannerTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  textBannerTitle: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    color: colors.white2,
  },
  containStep: {
    backgroundColor: colors.black2,
    width: 50,
    height: 15,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStep: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 8,
    color: colors.white2,
  },
  containInfo: {paddingHorizontal: 16},
  textDate: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 8,
    color: colors.white2,
  },
  containProgress: {width: 209, marginTop: 6},
});
