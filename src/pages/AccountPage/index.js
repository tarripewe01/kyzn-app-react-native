import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Picker,
} from 'react-native';

import Modal from 'react-native-modal';
import {TextInput} from 'react-native-paper';
import {
  ICEdit,
  ICEditBlue,
  ICFaq,
  ICLockBlack,
  ICLoyalty,
  ICQr,
} from '../../assets';
import {Divider, Gap} from '../../components';
import {fonts} from '../../utils';
import {colors} from '../../utils/Colors';
import HeaderAccount from './components/HeaderAccount';
import ListAccount from './components/ListAccount';

const imgModal = require('../../assets/Images/images_modal.png');

const AccountPage = () => {
  const navigation = useNavigation();
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const handleOnPress = () => {
    navigation.goBack('Account');
    toggleModal();
  };

  return (
    <>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.page}>
          <HeaderAccount
            toggleModal={toggleModal}
            isModalVisible={isModalVisible}
            onPress={handleOnPress}
          />

          <Modal isVisible={isModalVisible}>
            <ImageBackground style={{width: 350}} source={imgModal}>
              <TouchableOpacity>
                <View
                  style={{
                    marginTop: 20,
                    paddingHorizontal: 20,
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{width: 60, height: 60}}
                    source={require('../../assets/Images/image_profile.png')}
                  />
                  <View style={{marginTop: -15, marginLeft: 35}}>
                    <ICEditBlue />
                  </View>
                </View>
              </TouchableOpacity>

              <Gap height={40} />

              <View style={{paddingHorizontal: 20}}>
                <TextInput
                  mode="outlined"
                  label="Name"
                  theme={{
                    colors: {
                      primary: colors.grey8,
                      underlineColor: 'transparent',
                    },
                  }}
                  style={{
                    width: 310,
                    backgroundColor: colors.white1,
                    fontFamily: fonts.Montserrat[600],
                    fontSize: 12,
                    color: colors.grey3,
                    height: 50,
                  }}
                />
              </View>

              <View
                style={{
                  borderWidth: 1,
                  width: 310,
                  marginHorizontal: 20,
                  marginTop: 10,
                  borderRadius: 3,
                  borderColor: colors.grey3,
                  backgroundColor: colors.white1,
                  height: 50,
                }}>
                <Picker>
                  <Picker.Item
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 12,
                      color: colors.grey3,
                    }}
                    label="Male"
                    value="Male"
                  />
                  <Picker.Item
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 12,
                      color: colors.grey3,
                    }}
                    label="Female"
                    value="Female"
                  />
                </Picker>
              </View>

              <View style={{paddingHorizontal: 20}}>
                <TextInput
                  mode="outlined"
                  label="Birthday"
                  theme={{
                    colors: {
                      primary: colors.grey8,
                      underlineColor: 'transparent',
                    },
                  }}
                  style={{
                    width: 310,
                    backgroundColor: colors.white1,
                    fontFamily: fonts.Montserrat[600],
                    fontSize: 12,
                    color: colors.grey3,
                    height: 50,
                    marginTop: 3,
                  }}
                />
              </View>

              <View
                style={{
                  borderWidth: 1,
                  width: 310,
                  marginHorizontal: 20,
                  marginTop: 10,
                  borderRadius: 3,
                  borderColor: colors.grey3,
                  backgroundColor: colors.white1,
                  height: 50,
                }}>
                <Picker style={{}}>
                  <Picker.Item
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 12,
                      color: colors.grey3,
                    }}
                    label="Indonesian"
                    value="Indonesian"
                  />
                  <Picker.Item
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 12,
                      color: colors.grey3,
                    }}
                    label="Foreign"
                    value="Foreign"
                  />
                </Picker>
              </View>

              <View style={{paddingHorizontal: 20}}>
                <TextInput
                  mode="outlined"
                  label="Emergency Number"
                  theme={{
                    colors: {
                      primary: colors.grey8,
                      underlineColor: 'transparent',
                    },
                  }}
                  style={{
                    width: 310,
                    backgroundColor: colors.white1,
                    fontFamily: fonts.Montserrat[600],
                    fontSize: 12,
                    color: colors.grey3,
                    height: 50,
                    marginTop: 5,
                  }}
                />
              </View>

              <TouchableOpacity onPress={() => navigation.goBack('Account')}>
                <View
                  style={{
                    marginTop: 40,
                    backgroundColor: colors.black2,
                    width: 310,
                    height: 40,
                    marginHorizontal: 20,
                    borderRadius: 5,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 14,
                      color: colors.white2,
                    }}>
                    SAVE
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigation.goBack('Account')}>
                <View
                  style={{
                    marginTop: 5,
                    width: 310,
                    height: 40,
                    marginHorizontal: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 14,
                    }}>
                    Cancel
                  </Text>
                </View>
              </TouchableOpacity>

              <Gap height={20} />
            </ImageBackground>
          </Modal>

          <Gap height={75} />
          <View style={styles.containAdd}>
            <Text style={styles.titleAdd}>
              Add your kids to track their train Perform
            </Text>
            <TouchableOpacity onPress={toggleModal}>
              <View style={styles.containCta}>
                <Text style={styles.textCta}>+ Add Kids</Text>
              </View>
            </TouchableOpacity>
          </View>

          <Gap height={29} />
          <View style={{paddingHorizontal: 16}}>
            <Text
              style={{
                fontFamily: fonts.Montserrat[700],
                fontSize: 14,
                marginBottom: 16,
              }}>
              My Account
            </Text>

            <ListAccount
              icon={<ICEdit />}
              title="My Profile"
              onPress={() => navigation.navigate('Edit Account')}
            />
            <ListAccount
              icon={<ICLoyalty />}
              title="Loyalty Programs"
              onPress={() => navigation.navigate('Loyalty Program')}
            />
            <ListAccount icon={<ICLockBlack />} title="Change Password" />
            <ListAccount
              icon={<ICQr />}
              title="Show my QR"
              onPress={() => navigation.navigate('My QR')}
            />
            <Gap height={24} />
          </View>

          <View style={styles.divider}></View>

          <Gap height={29} />
          <View style={{paddingHorizontal: 16}}>
            <Text
              style={{
                fontFamily: fonts.Montserrat[700],
                fontSize: 14,
                marginBottom: 16,
              }}>
              About Account
            </Text>

            <ListAccount icon={<ICFaq />} title="FAQ" />
            <ListAccount icon={<ICEdit />} title="Term of Use" />
            <ListAccount icon={<ICEdit />} title="Privacy & Policy" />
            <Divider />
            <TouchableOpacity>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  padding: 16,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 12,
                      marginLeft: 25,
                    }}>
                    Version
                  </Text>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text
                    style={{
                      fontFamily: fonts.Montserrat[600],
                      fontSize: 12,
                      marginLeft: 5,
                    }}>
                    0.0.3
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            {/* <Gap height={24} /> */}
          </View>
        </View>
      </ScrollView>
    </>
  );
};

export default AccountPage;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  containAdd: {
    backgroundColor: colors.white1,
    height: 95,
    width: 350,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 18,
  },
  titleAdd: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    letterSpacing: 0.7,
  },
  containCta: {
    backgroundColor: colors.black2,
    width: 115,
    height: 30,
    padding: 5,
    marginTop: 15,
    alignItems: 'center',
    borderRadius: 5,
  },
  textCta: {
    color: colors.white2,
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    alignSelf: 'center',
  },
  divider: {backgroundColor: colors.white1, height: 8},
});
