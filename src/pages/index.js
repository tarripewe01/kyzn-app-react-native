import SplashPage from './SplashPage';
import OnboardingPage from './OnboardingPage';
import LoginPage from './LoginPage';
import RegisterPage from './RegisterPage';
import HomePage from './HomePage';
import ActivityPage from './ActivityPage';
import InboxPage from './InboxPage';
import AccountPage from './AccountPage';
import SearchPage from './SearchPage';
import WellnessPage from './WellnessPage';
import DetailClassPage from './DetailClassPage';
import TrainerPage from './TrainerPage';
import DetailTrainerPage from './DetailTrainerPage';
import SportsPage from './SportsPage';
import DetailSportsPage from './DetailSportsPage';
import PaymentPage from './PaymentPage';
import FitnessPage from './FitnessPage';
import WorkoutVideoPage from './WorkoutVideoPage';
import DetailFitness from './Detail Fitness';
import PurchaseVideoPage from './PurchaseVideoPage';
import WellnessCategoryPage from './WellnessCategoryPage';
import WellnessGroup from './WellnessGroupClasses';
import SportCategoryPage from './SportsCategoryPage';
import FitnessCategoryPage from './FitnessCategoryPage';
import AvailableSchedulePage from './AvailableSchedulePage';
import DetailActivityPage from './DetailActivityPage';
import DetailOrder from './DetailOrder';
import DetailInbox from './Detail Inbox';
import AllActivityPage from './AllActivityPage';
import PaymentHistory from './PaymentHistoryPage';
import EditAccountPage from './EditAccountPage';
import MyQRPage from './MyQRPage';
import MembershipPage from './MembershipPage';
import PremiumMembership from './PremuiumMembershipPage';
import DetailLoyaltyPrograms from './DetailLoyaltyPrograms';
import ZeniHistoryPage from './ZeniHistoryPage';

export {
  SplashPage,
  OnboardingPage,
  LoginPage,
  RegisterPage,
  HomePage,
  ActivityPage,
  InboxPage,
  AccountPage,
  SearchPage,
  WellnessPage,
  DetailClassPage,
  TrainerPage,
  DetailTrainerPage,
  SportsPage,
  DetailSportsPage,
  PaymentPage,
  FitnessPage,
  WorkoutVideoPage,
  DetailFitness,
  PurchaseVideoPage,
  WellnessCategoryPage,
  SportCategoryPage,
  WellnessGroup,
  FitnessCategoryPage,
  AvailableSchedulePage,
  DetailActivityPage,
  DetailOrder,
  DetailInbox,
  AllActivityPage,
  PaymentHistory,
  EditAccountPage,
  MyQRPage,
  MembershipPage,
  PremiumMembership,
  DetailLoyaltyPrograms,
  ZeniHistoryPage,
};
