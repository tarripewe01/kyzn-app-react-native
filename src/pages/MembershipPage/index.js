import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ProgressBar} from 'react-native-paper';
import {ICCheck} from '../../assets';
import {Gap} from '../../components';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {colors, fonts} from '../../utils';
import InfoMember from './InfoMember';

const MembershipPage = () => {
  const navigation = useNavigation();

  const Info = () => {
    return (
      <View style={{flexDirection: 'row'}}>
        <ICCheck />
        <Text
          style={{
            fontFamily: fonts.Montserrat[500],
            fontSize: 10,
            marginLeft: 5,
            marginBottom: 10,
          }}>
          Benefit lorem ipsum dolor sit amet
        </Text>
      </View>
    );
  };
  return (
    <>
      <View style={{flex: 1, backgroundColor: colors.white2}}>
        <ScrollView>
          <ImageBackground
            style={{width: '100%', height: 200}}
            source={require('../../assets/Images/image_banner_membership.png')}>
            <HeaderDetailClass onPress={() => navigation.goBack('Account')} />
            <View style={styles.containBannerTitle}>
              <Text style={styles.textBannerTitle}>3 Months Platinum Pass</Text>
              <View style={styles.containStep}>
                <Text style={styles.textStep}>4x LEFT</Text>
              </View>
            </View>
            <View style={styles.containInfo}>
              <Text style={styles.textDate}>
                START 21 DEC 2020 - END 21 JAN 2021
              </Text>
              <View style={styles.containProgress}>
                <ProgressBar progress={0.7} color={colors.white2} />
              </View>
            </View>
          </ImageBackground>

          <View style={{paddingHorizontal: 20, marginTop: 24}}>
            <Text
              style={{
                fontFamily: fonts.Montserrat[700],
                fontSize: 10,
                color: colors.grey9,
              }}>
              MEMBERSHIP INFO
            </Text>
          </View>

          <Gap height={16} />
          <InfoMember title="Period" subtitle="3 Month" type="1" />
          <InfoMember title="Start Date" subtitle="20 Jan 2020" />
          <InfoMember title="Expired Date" subtitle="20 Mar 2021" type="1" />

          <View style={{paddingHorizontal: 20, marginTop: 32}}>
            <Text
              style={{
                fontFamily: fonts.Montserrat[700],
                fontSize: 10,
                color: colors.grey9,
              }}>
              RECURRING INFO
            </Text>
          </View>

          <Gap height={16} />
          <InfoMember
            title="Status"
            subtitle="Active"
            type="1"
            active="active"
          />
          <InfoMember title="Amount" subtitle="Rp 5.800.000" />
          <InfoMember
            title="Last recurring"
            subtitle="Sun, 23 Jan 2020"
            type="1"
          />
          <InfoMember title="Next recurring" subtitle="15 Jan 2021" />
          <InfoMember
            title="Payment Using"
            subtitle="Virtual Account"
            type="1"
          />
          <InfoMember title="Bank" icon="icon" />

          <Gap height={32} />
          <TouchableOpacity
            onPress={() => navigation.navigate('Premium Membership')}>
            <View
              style={{
                width: 350,
                height: 34,
                backgroundColor: colors.black1,
                marginHorizontal: 20,
                borderRadius: 5,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: fonts.Montserrat[700],
                  fontSize: 14,
                  color: colors.white2,
                }}>
                Renew
              </Text>
            </View>
          </TouchableOpacity>

          <Gap height={15} />
          <TouchableOpacity onPress={() => navigation.goBack('Account')}>
            <View
              style={{
                width: 350,
                height: 34,
                marginHorizontal: 20,
                borderRadius: 5,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: fonts.Montserrat[600],
                  fontSize: 12,
                }}>
                Stop Recurring
              </Text>
            </View>
          </TouchableOpacity>

          <View
            style={{
              borderWidth: 1,
              borderColor: colors.grey2,
              width: 350,
              marginHorizontal: 20,
              borderRadius: 5,
              paddingHorizontal: 10,
              marginTop: 24,
              paddingTop: 16,
            }}>
            <Text
              style={{
                fontFamily: fonts.Montserrat[600],
                fontSize: 12,
                color: colors.grey9,
                marginBottom: 15,
              }}>
              What you get :
            </Text>
            <Info />
            <Info />
            <Info />
            <Info />
            <Info />
          </View>

          <Gap height={30} />
        </ScrollView>
      </View>
    </>
  );
};

export default MembershipPage;

const styles = StyleSheet.create({
  containBannerTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  textBannerTitle: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    color: colors.white2,
    marginTop: 24,
  },
  containStep: {
    backgroundColor: colors.black2,
    width: 50,
    height: 15,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  textStep: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 8,
    color: colors.white2,
  },
  containInfo: {paddingHorizontal: 16},
  textDate: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 8,
    color: colors.white2,
  },
  containProgress: {width: 209, marginTop: 6},
});
