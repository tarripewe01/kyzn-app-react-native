import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ICBCA} from '../../assets';
import {colors, fonts} from '../../utils';

const InfoMember = ({title, subtitle, type, active, icon}) => {
  return (
    <View style={styles.page(type)}>
      <Text style={styles.title}>{title}</Text>
      {icon === 'icon' ? (
        <ICBCA />
      ) : (
        <Text style={styles.subtitle(active)}>{subtitle}</Text>
      )}
    </View>
  );
};

export default InfoMember;

const styles = StyleSheet.create({
  page: type => ({
    backgroundColor: type === '1' ? colors.white1 : null,
    width: 360,
    height: 40,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    alignItems: 'center',
  }),
  title: {fontFamily: fonts.Montserrat[500], fontSize: 12},
  subtitle: active => ({
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: active === 'active' ? colors.green2 : null,
  }),
});
