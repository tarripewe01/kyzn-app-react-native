import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import {ICEmail, ICGoogle, ICLock} from '../../assets';
import {Bar, Gap} from '../../components';
import {colors} from '../../utils/Colors';
import {fonts} from '../../utils/Fonts';

const logoLogin = require('../../assets/Images/images_login.png');
const {width, height} = Dimensions.get('window');

const LoginPage = ({navigation, login, register}) => {
  return (
    <>
      <Bar />
      <View style={styles.page}>
        <View style={styles.containLogo}>
          <Image source={logoLogin} style={styles.logo} />
        </View>

        <View style={styles.containTitle}>
          <Text style={styles.title}>
            Improve quality live with a healthy lifestyle, starts here
          </Text>
        </View>

        <Gap height={40} />

        <View style={styles.containInput}>
          <View style={styles.input}>
            <ICEmail />
            <TextInput placeholder="Email" style={styles.textInput} />
          </View>
        </View>

        <Gap height={24} />

        <View style={styles.containInput}>
          <View style={styles.input}>
            <ICLock />
            <TextInput
              secureTextEntry
              placeholder="Password"
              style={styles.textInput}
            />
          </View>
        </View>

        <Gap height={24} />

        <TouchableOpacity onPress={login} style={styles.containButton}>
          <Text style={styles.textButton}>Login</Text>
        </TouchableOpacity>

        <Gap height={40} />

        <View style={styles.divider}></View>

        <Gap height={40} />

        <TouchableOpacity style={styles.containBtnGoogle}>
          <View style={styles.google}>
            <ICGoogle />
            <Text style={styles.textBtnGoogle}>Login via Google</Text>
          </View>
        </TouchableOpacity>

        <Gap height={40} />

        <View style={styles.link}>
          <TouchableOpacity onPress={register}>
            <Text>Create Account</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text>Forgot Password</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1, paddingHorizontal: 38},
  containLogo: {alignItems: 'center', marginTop: 80},
  logo: {
    width: Platform.OS === 'android' ? width / 2.5 : width / 2.3,
    height: height / 14.5,
  },
  containTitle: {marginTop: 50, paddingLeft: 12, width: width / 1.3},
  title: {
    fontSize: height / 50,
    fontFamily: fonts.Montserrat[600],
    lineHeight: height / 30,
  },
  containInput: {
    borderWidth: 1,
    borderColor: colors.grey1,
    width: width / 1.25,
    height: height / 15,
    borderRadius: 5,
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 12,
    height: Platform.OS === 'ios' ? 55 : 45,
  },
  textInput: {
    marginLeft: 6,
    fontSize: height / 60,
    fontFamily: fonts.Montserrat[500],
    flex: 1,
  },
  containButton: {
    backgroundColor: colors.blue1,
    height: height / 15,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    fontSize: height / 50,
    fontFamily: fonts.Montserrat[700],
    color: colors.white1,
  },
  divider: {borderWidth: 1, borderColor: colors.grey2},
  containBtnGoogle: {
    borderWidth: 1,
    borderColor: colors.black1,
    height: height / 15,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  google: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtnGoogle: {
    fontSize: height / 50,
    fontFamily: fonts.Montserrat[600],
    marginLeft: 4,
  },
  link: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 80,
  },
});
