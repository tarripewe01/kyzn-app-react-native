import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { ICArrowDwon, ICArrowNext, ICNotif } from '../../../assets';
import { Gap } from '../../../components';
import { colors, fonts } from '../../../utils';
import CardVideo from '../components';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from 'react-native-responsive-screen';

const AllVideo = () => {
  return (
    <View style={styles.page}>
      <Gap height={24} />
      <View style={{paddingHorizontal: 20}}>
        <View
          style={styles.containNotif}>
          <View style={styles.iconNotif}>
            <ICNotif />
            <Text
              style={styles.titleNotif}>
              Purchased Video
            </Text>
          </View>
          <View style={{marginRight: 10}}>
            <ICArrowNext />
          </View>
        </View>
      </View>

      <Gap height={16} />
      <View style={styles.contentText}>
        <Text style={styles.title}>Showing all 20 classes</Text>
        <TouchableOpacity style={styles.contentAction}>
          <Text style={styles.sort}>Sort by</Text>
          <ICArrowDwon />
        </TouchableOpacity>
      </View>
      <Gap height={20} />
      <ScrollView>
        <View style={{flexDirection: 'row', paddingHorizontal: 20}}>
          <CardVideo
            img={require('../../../assets/Dummy/dummy_video1.png')}
            title="Yoga for Beginner"
            price="299.000"
          />
          <CardVideo
            img={require('../../../assets/Dummy/dummy_video2.png')}
            title="Yoga for Beginner"
            price="299.000"
          />
        </View>
        <Gap height={20} />
        <View style={{flexDirection: 'row', paddingHorizontal: 20}}>
          <CardVideo
            img={require('../../../assets/Dummy/dummy_video1.png')}
            title="Yoga for Beginner"
            price="299.000"
          />
          <CardVideo
            img={require('../../../assets/Dummy/dummy_video3.png')}
            title="Yoga for Beginner"
            price="299.000"
          />
        </View>

        <Gap height={20} />
        <View style={{flexDirection: 'row', paddingHorizontal: 20}}>
          <CardVideo
            img={require('../../../assets/Dummy/dummy_video1.png')}
            title="Yoga for Beginner"
            price="299.000"
          />
          <CardVideo
            img={require('../../../assets/Dummy/dummy_video2.png')}
            title="Yoga for Beginner"
            price="299.000"
          />
        </View>

        <Gap height={50} />
      </ScrollView>
    </View>
  );
};

export default AllVideo;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
  contentText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 22,
    paddingRight: 18,
  },
  title: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 12,
    color: colors.grey9,
  },
  contentAction: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  sort: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    marginRight: 5,
  },
  containNotif: {
    borderWidth: 1,
    borderColor: colors.grey4,
    backgroundColor: colors.blue3,
    width: wp('92%'),
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 5,
  },
  iconNotif: {flexDirection: 'row', paddingHorizontal: 10},
  titleNotif: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 11,
    marginLeft: 10,
  },
});
