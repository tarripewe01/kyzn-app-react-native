import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {ICArrowBack} from '../../assets';
import {Bar} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../utils';
import CardioFitness from '../FitnessPage/CardioFitness';
import FatLossFitness from '../FitnessPage/FatLossFitness';
import {SwimmingClass, TennisClass} from '../WellnessPage/Classes';
import AllVideo from './AllVideo';

const Tab = createMaterialTopTabNavigator();

const WorkoutVideoPage = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.page}>
      <Bar />
      <View style={{marginTop: 20}}>
        <HeaderCourse
          icon={<ICArrowBack />}
          title="Workout Video"
          onPress={() => navigation.navigate('MainApp')}
        />
      </View>

      <Tab.Navigator
        screenOptions={{
          tabBarLabelStyle: {fontSize: 12, fontFamily: fonts.Montserrat[700]},
          tabBarStyle: {width: '100%', paddingTop: 15},
          tabBarAllowFontScaling: true,
          tabBarLabelStyle: {textTransform: 'none'},
          tabBarIndicatorStyle: {backgroundColor: colors.black1},
          tabBarItemStyle: {width: 100, paddingHorizontal: -10, paddingVertical: -10},
          tabBarScrollEnabled: true,
        }}>
        <Tab.Screen name="All" component={AllVideo} />
        <Tab.Screen name="Cardio" component={CardioFitness} />
        <Tab.Screen name="Fat Loss" component={FatLossFitness} />
        <Tab.Screen name="Tennis" component={TennisClass} />
        <Tab.Screen name="Swimming" component={SwimmingClass} />
      </Tab.Navigator>
    </View>
  );
};

export default WorkoutVideoPage;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1}
});
