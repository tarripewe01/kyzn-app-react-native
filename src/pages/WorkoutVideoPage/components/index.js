import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import {colors, fonts} from '../../../utils';
import {ICTimer} from '../../../assets';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const CardVideo = ({title, price, img}) => {
  const navigation = useNavigation();
  return (
    <>
      <TouchableOpacity onPress={() => navigation.navigate('Purchase Video')}>
        <View style={{marginRight: 10}}>
          <View style={styles.card}>
            <Image style={styles.imgcourse} source={img} />
            <View style={styles.contain}>
              <Text style={styles.title}>{title}</Text>
              <View style={styles.content}>
                <View>
                  <Text style={styles.price}>Rp {price}</Text>
                </View>
                <View style={styles.timer}>
                  <View>
                    <ICTimer />
                  </View>
                  <Text style={styles.time}>15Mins</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default CardVideo;

const styles = StyleSheet.create({
  card: {
    width: wp('45%'),
    height: Platform.OS === 'android' ? hp('33%') : hp('27%'),
    borderWidth: 1,
    borderColor: colors.grey4,
  },
  imgcourse: {width: wp('45%'), height: 158},
  contain: {marginTop: 12, paddingHorizontal: 12},
  title: {fontFamily: fonts.Montserrat[700], fontSize: 12},
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  price: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 10,
    marginTop: 8,
  },
  timer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  time: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 10,
    color: colors.grey7,
    marginLeft: 3,
  },
});
