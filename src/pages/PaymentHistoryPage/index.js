import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {Bar, Divider, Gap} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import OrderList from '../ActivityPage/components/OrderList';
import {colors, fonts} from '../../utils';

const PaymentHistory = () => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: colors.white2}}>
      <Bar />
      <ScrollView>
        <View style={{marginTop: 20}}>
          <HeaderCourse
            onPress={() => navigation.goBack('Activity')}
            title="Payment History"
          />
        </View>

        <View
          style={{
            backgroundColor: colors.white1,
            paddingHorizontal: 20,
            marginTop: 20,
            height: 38,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontFamily: fonts.Montserrat[600],
              fontSize: 10,
              color: colors.grey3,
            }}>
            FEBRUARY 2021
          </Text>
        </View>

        <Gap height={24} />
        <Divider />
        <Gap height={16} />
        <OrderList
          course="Yoga for Beginner"
          payment="BCA Virtual Account"
          date="2 Feb 2021"
          price="520.000"
          status="PENDING"
          type="PENDING"
        />

        <Gap height={24} />
        <Divider />
        <Gap height={16} />
        <OrderList
          course="3 Months Platinum Pass"
          payment="Credit Card"
          date="2 Feb 2021"
          price="499.000"
          status="COMPLETED"
          type="COMPLETED"
        />

        <Gap height={24} />
        <Divider />
        <Gap height={16} />
        <OrderList
          course="Yoga for Beginner"
          payment="BCA Virtual Account"
          date="2 Feb 2021"
          price="520.000"
          status="PENDING"
          type="PENDING"
        />

        <Gap height={24} />
        <Divider />
        <Gap height={16} />
        <OrderList
          course="3 Months Platinum Pass"
          payment="Credit Card"
          date="2 Feb 2021"
          price="499.000"
          status="COMPLETED"
          type="COMPLETED"
        />

        <View
          style={{
            backgroundColor: colors.white1,
            paddingHorizontal: 20,
            marginTop: 20,
            height: 38,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontFamily: fonts.Montserrat[600],
              fontSize: 10,
              color: colors.grey3,
            }}>
            JANUARY 2021
          </Text>
        </View>

        <Gap height={24} />
        <Divider />
        <Gap height={16} />
        <OrderList
          course="Yoga for Beginner"
          payment="BCA Virtual Account"
          date="2 Feb 2021"
          price="520.000"
          status="PENDING"
          type="PENDING"
        />

        <Gap height={24} />
        <Divider />
        <Gap height={16} />
        <OrderList
          course="3 Months Platinum Pass"
          payment="Credit Card"
          date="2 Feb 2021"
          price="499.000"
          status="COMPLETED"
          type="COMPLETED"
        />

        <Gap height={24} />
        <Divider />
        <Gap height={16} />
        <OrderList
          course="Yoga for Beginner"
          payment="BCA Virtual Account"
          date="2 Feb 2021"
          price="520.000"
          status="PENDING"
          type="PENDING"
        />

        <Gap height={24} />
        <Divider />
        <Gap height={16} />
        <OrderList
          course="3 Months Platinum Pass"
          payment="Credit Card"
          date="2 Feb 2021"
          price="499.000"
          status="COMPLETED"
          type="COMPLETED"
        />

        <Gap height={71} />
      </ScrollView>
    </View>
  );
};

export default PaymentHistory;

const styles = StyleSheet.create({});
