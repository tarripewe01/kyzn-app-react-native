import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  View,
  Platform,
} from 'react-native';
import {ICArrowNext, ICBCA} from '../../assets';
import {Bar, Gap} from '../../components';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {colors, fonts} from '../../utils';

const DetailOrder = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.page}>
      <ScrollView>
        <Bar />
        <View style={{marginTop: 20}}>
          <HeaderDetailClass title="Detail Order" />
        </View>

        <View style={styles.info}>
          <View>
            <Text style={styles.status}>Payment Status</Text>
            <Text style={styles.textStatus}>PENDING</Text>
          </View>
        </View>

        <View style={styles.containPayment}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={styles.textPayment}>BCA Virtual Account</Text>
              <View style={{flexDirection: 'row', marginTop: 5}}>
                <ICBCA />
                <Text style={styles.account}>013904293482349</Text>
              </View>
            </View>

            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <TouchableOpacity>
                <ICArrowNext />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={{paddingHorizontal: 20, marginTop: 32}}>
          <View style={styles.contain}>
            <Text style={styles.title}>Order ID</Text>
            <Text style={styles.title}>2387429</Text>
          </View>
          <View style={styles.contain}>
            <Text style={styles.title}>Date & Time</Text>
            <Text style={styles.title}> 23 Jan 2019 | 20:00</Text>
          </View>
          <View style={styles.contain}>
            <Text style={styles.title}>Payment Method</Text>
            <Text style={styles.title}> Bca Virtual Accounts</Text>
          </View>
          <View style={styles.contain}>
            <Text style={styles.title}>VA Number</Text>
            <Text style={styles.title}>08123234353453</Text>
          </View>
        </View>

        <Gap height={24} />
        <View style={styles.border}></View>

        <View style={styles.containTitle}>
          <Text style={styles.product}>Product</Text>
          <Text style={styles.price}>Price</Text>
        </View>

        <View style={styles.containInfoPayment}>
          <View>
            <Text style={styles.masaBerlaku}>3 Months Platinum Pass</Text>
            <Text style={styles.rentangWaktu}>Period 3 Apr - 5 May 2021</Text>
          </View>
          <View style={{justifyContent: 'center'}}>
            <Text style={styles.harga}>RP 499.999</Text>
          </View>
        </View>

        <View style={styles.containDiscount}>
          <Text style={styles.discount}>Discount</Text>
          <View style={{justifyContent: 'center'}}>
            <Text style={styles.textDiscount}>-</Text>
          </View>
        </View>

        <View style={styles.containTotal}>
          <View>
            <Text style={styles.textTotal}>Total</Text>
          </View>
          <View style={{justifyContent: 'center', marginLeft: 45}}>
            <Text style={styles.totalPrice}>RP 499.999</Text>
          </View>
        </View>

        <Gap height={100} />

        <TouchableOpacity onPress={() => navigation.navigate('MainApp')}>
          <View style={styles.containButton}>
            <Text style={styles.textButton}>OK</Text>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default DetailOrder;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  info: {
    paddingHorizontal: 20,
    marginTop: 20,
  },
  status: {fontFamily: fonts.Montserrat[600], fontSize: 11},
  textStatus: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.orange2,
  },
  containPayment: {
    borderWidth: 1,
    borderColor: colors.grey4,
    paddingHorizontal: 16,
    paddingVertical: 12,
    width: Platform.OS === 'android' ? 370 : 360,
    justifyContent: 'center',
    marginTop: 20,
    marginHorizontal: 20,
  },
  textPayment: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey3,
  },
  title: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
  },
  account: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 14,
    marginLeft: 5,
  },
  contain: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  border: {backgroundColor: colors.grey4, height: 5},
  containTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingTop: 20,
  },
  product: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey3,
  },
  price: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey3,
    marginRight: 35,
  },
  containInfoPayment: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 12,
    backgroundColor: colors.grey11,
    width: 350,
    marginHorizontal: 20,
    height: 52,
    paddingHorizontal: 10,
    paddingVertical: 12,
  },
  masaBerlaku: {fontFamily: fonts.Montserrat[600], fontSize: 12},
  rentangWaktu: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey3,
  },
  harga: {fontFamily: fonts.Montserrat[700], fontSize: 12},
  containDiscount: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 350,
    marginHorizontal: 20,
    height: 52,
    paddingHorizontal: 10,
    paddingVertical: 12,
  },
  discount: {fontFamily: fonts.Montserrat[600], fontSize: 12},
  textDiscount: {fontFamily: fonts.Montserrat[700], fontSize: 12},
  containTotal: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 12,
    backgroundColor: colors.grey11,
    width: 350,
    marginHorizontal: 20,
    height: 52,
    paddingHorizontal: 10,
    paddingVertical: 12,
    alignItems: 'center',
  },
  textTotal: {fontFamily: fonts.Montserrat[600], fontSize: 12},
  totalPrice: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    color: colors.grey3,
  },
  containButton: {
    backgroundColor: colors.black2,
    width: 350,
    alignItems: 'center',
    height: 45,
    justifyContent: 'center',
    marginHorizontal: 20,
    borderRadius: 5,
    marginTop: Platform.OS === 'ios' ? 70 : 0,
  },
  textButton: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 14,
    color: colors.white2,
  },
});
