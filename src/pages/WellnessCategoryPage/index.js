import {useNavigation} from '@react-navigation/native';
import React, {useRef, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ListItem} from 'react-native-elements';
import {
  ICBeautySpa,
  ICHealing,
  ICMedicine,
  ICMeditation,
  ICPilates,
  ICRecovery,
  ICSports,
  ICWellness,
  ICYoga,
} from '../../assets';
import {Bar, Gap} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../utils';
import RBSheet from 'react-native-raw-bottom-sheet';
import SelectServices from './SelectServices';

const Category = [
  {
    id: 1,
    icon: <ICYoga />,
    title: 'Yoga',
  },
  {
    id: 2,
    icon: <ICMeditation />,
    title: 'Meditation',
  },
  {
    id: 3,
    icon: <ICPilates />,
    title: 'Pilates',
  },
  {
    id: 4,
    icon: <ICRecovery />,
    title: 'Recovery',
  },
  {
    id: 5,
    icon: <ICMedicine />,
    title: 'Functional Medicine',
  },
  {
    id: 6,
    icon: <ICBeautySpa />,
    title: 'Beauty & Spa',
  },
  {
    id: 7,
    icon: <ICHealing />,
    title: 'Healing',
  },
];

const WellnessCategoryPage = () => {
  const navigation = useNavigation();

  const [visible, setVisible] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const refRBSheet = useRef();

  const handleBottomSheet = () => refRBSheet.current.open();

  const handleGroupClass = () => {
    navigation.navigate('Wellness'), refRBSheet.current.close();
  };

  const handlePrivateTraining = () => {
    navigation.navigate('Trainer'), refRBSheet.current.close();
  };

  return (
    <View style={styles.page}>
      <Bar />
      <View>
        <HeaderCourse
          title="Wellness"
          onPress={() => navigation.navigate('MainApp')}
        />
      </View>

      <Gap height={30} />
      {Category.map((item, i) => (
        <TouchableOpacity onPress={handleBottomSheet}>
          <RBSheet
            height={250}
            ref={refRBSheet}
            closeOnDragDown={true}
            closeOnPressMask={true}>
            <View>
              <View style={styles.contentText}>
                <Text style={styles.text}>Select Services</Text>
              </View>
              <SelectServices
                icon={<ICWellness />}
                title="Group Class"
                onPress={handleGroupClass}
              />
              <SelectServices
                icon={<ICSports />}
                title="Private Training"
                onPress={handlePrivateTraining}
              />
            </View>
          </RBSheet>

          <ListItem key={item.id} style={styles.containlist}>
            <View style={styles.icon}>{item.icon}</View>
            <ListItem.Content>
              <ListItem.Title style={styles.title}>{item.title}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default WellnessCategoryPage;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  contentText: {
    paddingHorizontal: 20,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {fontSize: 18, fontWeight: '700'},
  containlist: {paddingHorizontal: 20, marginBottom: 5},
  icon: {width: 40, height: 40, alignItems: 'center'},
  title: {fontFamily: fonts.Montserrat[700], fontSize: 14},
});
