import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {fonts} from '../../utils';

const SelectServices = ({onPress, icon, title}) => {
  return (
    <View style={styles.page}>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.icon}>
          {icon}
          <Text style={styles.title}>{title}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default SelectServices;

const styles = StyleSheet.create({
  page: {paddingHorizontal: 10, marginBottom: 20},
  icon: {flexDirection: 'row', alignItems: 'center'},
  title: {
    marginLeft: 10,
    fontFamily: fonts.Montserrat[700],
    fontSize: 14,
  },
});
