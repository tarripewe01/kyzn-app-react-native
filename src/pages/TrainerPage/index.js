import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Bar} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../utils';
import {SwimmingClass, TennisClass} from '../WellnessPage/Classes';
import AllTraining from './AllTraining';
import Cardio from './Cardio';
import FatLoss from './Fat Loss';

const Tab = createMaterialTopTabNavigator();

const TrainerPage = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.page}>
      <Bar />
      <View style={styles.containHeader}>
        <HeaderCourse
          title="Personal Trainer"
          onPress={() => navigation.navigate('MainApp')}
        />
      </View>

      <Tab.Navigator
        screenOptions={{
          tabBarLabelStyle: {fontSize: 12, fontFamily: fonts.Montserrat[700]},
          tabBarStyle: {width: '100%', paddingTop: 10},
          tabBarAllowFontScaling: true,
          tabBarLabelStyle: {textTransform: 'none'},
          tabBarIndicatorStyle: {backgroundColor: colors.black1},
          tabBarItemStyle: {
            width: 100,
            paddingHorizontal: -10,
            paddingVertical: -10,
          },
          tabBarScrollEnabled: true,
        }}>
        <Tab.Screen name="All" component={AllTraining} />
        <Tab.Screen name="Cardio" component={Cardio} />
        <Tab.Screen name="Fat Loss" component={FatLoss} />
        <Tab.Screen name="Tennis" component={TennisClass} />
        <Tab.Screen name="Swimming" component={SwimmingClass} />
      </Tab.Navigator>
    </View>
  );
};

export default TrainerPage;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  containHeader: {marginTop: 20},
});
