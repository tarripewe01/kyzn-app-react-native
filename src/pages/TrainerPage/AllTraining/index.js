import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ICArrowDwon} from '../../../assets';
import {Gap} from '../../../components';
import {colors, fonts} from '../../../utils';
import CardTrainer from '../components/CardTrainer';

const AllTraining = () => {
  return (
    <View style={styles.page}>
      <ScrollView>
        <Gap height={24} />
        <View style={styles.contentText}>
          <Text style={styles.title}>Showing all 20 classes</Text>
          <TouchableOpacity style={styles.contentAction}>
            <Text style={styles.sort}>Sort by</Text>
            <ICArrowDwon />
          </TouchableOpacity>
        </View>
        <Gap height={24} />
        <View>
          <CardTrainer />
          <CardTrainer />
          <CardTrainer />
          <CardTrainer />
          <CardTrainer />
          <CardTrainer />
          <CardTrainer />
        </View>
        <Gap height={24} />
      </ScrollView>
    </View>
  );
};

export default AllTraining;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
  contentText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 22,
    paddingRight: 18,
  },
  title: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 12,
    color: colors.grey9,
  },
  contentAction: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  sort: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    marginRight: 5,
  },
});
