import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Gap} from '../../../components';
import {colors, fonts} from '../../../utils';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const imgTrainer = require('../../../assets/Dummy/dummy_trainer2.png');
const imgStar = require('../../../assets/Images/image_star.png');

const CardTrainer = () => {
  const navigation = useNavigation();
  return (
    <>
      <TouchableOpacity onPress={() => navigation.navigate('Detail Trainer')}>
        <View style={styles.page}>
          <View style={styles.container}>
            <Image style={styles.imgTrainer} source={imgTrainer} />
            <View style={styles.containerTitle}>
              <Text style={styles.name}>Khazan Jacob</Text>
              <Text style={styles.course}>Cardio & 3 Others</Text>
              <View style={styles.containersubtitle}>
                <View style={styles.containPrice}>
                  <Text style={styles.start}>Starts from</Text>
                  <Text style={styles.price}>Rp 250.000</Text>
                </View>
                <View style={styles.containRate}>
                  <Image style={styles.imgStar} source={imgStar} />
                  <Text style={styles.rating}>4.7</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default CardTrainer;

const styles = StyleSheet.create({
  page: {
    height: 106,
    borderWidth: 1,
    borderColor: colors.grey4,
    paddingTop: 10,
  },
  container: {paddingHorizontal: 20, flexDirection: 'row'},
  imgTrainer: {width: 50, height: 50, borderRadius: 3},
  containerTitle: {marginLeft: 16, marginRight: 16},
  name: {fontFamily: fonts.Montserrat[700], fontSize: 13},
  course: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 11,
    color: colors.grey3,
  },
  containersubtitle: {
    flexDirection: 'row',
    marginTop: 28,
    justifyContent: 'space-between',
    width: wp('72%'),
  },
  containPrice: {flexDirection: 'row', marginTop: -10},
  start: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 13,
    color: colors.grey3,
  },
  price: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 13,
    marginLeft: 5,
  },
  containRate: {flexDirection: 'row', marginTop: -10},
  imgStar: {width: 12, height: 12},
  rating: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 11,
    marginLeft: 5,
  },
});
