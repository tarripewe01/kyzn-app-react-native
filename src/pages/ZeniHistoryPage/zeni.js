import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {BackgroundImage} from 'react-native-elements/dist/config';
import {Divider} from '../../components';
import {colors, fonts} from '../../utils';

const Zeni = ({item, mutation, balance, type}) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: 20,
          marginTop: 15,
          marginBottom: 15,
        }}>
        <View style={{}}>
          <Text style={styles.item}>{item}</Text>
          <Text style={styles.textHeader}>24 Jun 2021</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: 150,
          }}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: 15,
            }}>
            <Text style={styles.textmutation(type)}>{mutation}</Text>
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginRight: 15,
            }}>
            <Text style={styles.textbalance}>{balance}</Text>
          </View>
        </View>
      </View>
      <Divider />
    </>
  );
};

export default Zeni;

const styles = StyleSheet.create({
  textHeader: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 10,
    color: colors.grey3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {fontFamily: fonts.Montserrat[700], fontSize: 12},
  textmutation: type => ({
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: type === 'minus' ? colors.red2 : null,
  }),
  textbalance: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.blue1,
  },
});
