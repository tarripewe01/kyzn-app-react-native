import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../utils';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {Bar, Divider} from '../../components';
import zeni from './zeni';
import Zeni from './zeni';

const ZeniHistoryPage = () => {
  const navigation = useNavigation();
  return (
    <ScrollView style={{flex: 1, backgroundColor: colors.white2}}>
      <Bar />
      <View style={{marginTop: 20}}>
        <HeaderCourse
          title="Zeni History"
          onPress={() => navigation.goBack('Loyalty Program')}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: 20,
          marginTop: 30,
          marginBottom: 15,
        }}>
        <View style={{}}>
          <Text style={styles.textHeader}>Item Name</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: 150,
          }}>
          <Text style={styles.textHeader}>Mutation</Text>
          <Text style={styles.textHeader}>Balance</Text>
        </View>
      </View>
      <Divider />
      <Zeni item="Happy Birthday Claim" mutation="+50" balance="150" />
      <Zeni
        item="Redeem  Basketball"
        mutation="-50"
        balance="100"
        type="minus"
      />
      <Zeni item="Redeem  Footbal" mutation="-30" balance="70" type="minus" />
      <Zeni item="Stand Achievement" mutation="+50" balance="120" />
      <Zeni item="Move Achievement" mutation="+50" balance="170" />
    </ScrollView>
  );
};

export default ZeniHistoryPage;

const styles = StyleSheet.create({
  textHeader: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 10,
    color: colors.grey3,
  },
});
