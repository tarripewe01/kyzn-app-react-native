import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {colors} from '../../../utils/Colors';
import {fonts} from '../../../utils/Fonts';

const imageBg = require('../../../assets/Images/image_cardActivity.png');

const CardUpcoming = () => {
  return (
    <View style={styles.page}>
      <ImageBackground source={imageBg} style={styles.card}>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.container}>
            <View style={styles.containTitle}>
              <Text style={styles.textDay}>Tommorrow</Text>
              <View style={styles.dot}></View>
              <Text style={styles.time}>10:00</Text>
            </View>
            <Text style={styles.course}>YOGA for Beginner</Text>
          </View>
          <View>
            <View style={styles.containCategory}>
              <Text style={styles.textCategory}>WELLNESS</Text>
            </View>
          </View>
        </View>
        <View style={{marginTop: 8}}>
          <Text style={styles.textStatus}>CONFIRMED</Text>
        </View>
      </ImageBackground>
    </View>
  );
};

export default CardUpcoming;

const styles = StyleSheet.create({
  page: {
    paddingLeft: 16,
  },
  card: {width: 328, height: 85, paddingHorizontal: 16},
  container: {
    width: 235,
    height: 35,
    marginTop: 16,
  },
  containTitle: {flexDirection: 'row', alignItems: 'center'},
  textDay: {
    fontFamily: fonts.Rubik[500],
    fontSize: 10,
    marginRight: 5,
    color: colors.grey3,
  },
  dot: {
    height: 5,
    width: 5,
    borderRadius: 50,
    backgroundColor: colors.grey3,
    marginRight: 5,
  },
  time: {
    fontFamily: fonts.Rubik[500],
    fontSize: 10,
    marginRight: 5,
    color: colors.grey3,
  },
  course: {
    fontFamily: fonts.Rubik[700],
    fontSize: 12,
    marginTop: 8,
    lineHeight: 14,
    letterSpacing: 1,
  },
  containCategory: {
    height: 16,
    width: 65,
    backgroundColor: colors.black2,
    alignItems: 'center',
    marginTop: 20,
    justifyContent: 'center',
    borderRadius: 3,
  },
  textCategory: {
    color: 'white',
    fontFamily: fonts.Rubik[700],
    fontSize: 8,
  },
  textStatus: {
    fontFamily: fonts.Rubik[700],
    fontSize: 8,
    color: colors.green2,
  },
});
