import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors} from '../../../utils/Colors';
import {fonts} from '../../../utils/Fonts';

const imageXp = require('../../../assets/Images/image_xp.png');

const CardCourse = ({course, title, price, xp, onPress}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.page}
      onPress={onPress}>
      <View>
        <Image source={course} style={styles.imageCourse} />
      </View>
      <View style={styles.contain}>
        <Text style={styles.title} numberOfLines={1}>
          {title}
        </Text>
        <Text style={styles.price}>Rp {price}</Text>
        <View style={styles.containXp}>
          <Image source={imageXp} style={styles.imageXp} />
          <Text style={styles.textXp}>{xp}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CardCourse;

const styles = StyleSheet.create({
  page: {
    width: 140,
    height: 200,
    borderWidth: 1,
    borderColor: colors.grey4,
    borderRadius: 5,
    shadowColor: '#000',
    marginLeft: 16,
    marginRight: -10,
  },
  imageCourse: {width: 140, height: 104},
  contain: {paddingLeft: 8, marginTop: 11},
  title: {fontFamily: fonts.Rubik[500], fontSize: 12},
  price: {
    fontFamily: fonts.Rubik[700],
    fontSize: 10,
    marginTop: 10,
  },
  containXp: {flexDirection: 'row', alignItems: 'center'},
  imageXp: {width: 15, height: 14, marginRight: 5, marginTop: 4},
  textXp: {
    fontFamily: fonts.Rubik[500],
    fontSize: 10,
    marginTop: 6,
    alignItems: 'center',
  },
});
