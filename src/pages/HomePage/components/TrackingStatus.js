import React, {useState} from 'react';
import {
  Button,
  LayoutAnimation,
  Platform,
  StyleSheet,
  Text,
  UIManager,
  View,
} from 'react-native';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

const status = [
  {
    id: 1,
    level: 'level 1',
    xp: 1000,
  },
  {
    id: 2,
    level: 'level 2',
    xp: 2000,
  },
  // {
  //   id: 3,
  //   level: 'level 3',
  //   xp: 3000,
  // },
  // 'level 1',
  // 'level 2',
  // 'level 3',
  // 'level 4',
  //   'level 5',
  //   'level 6',
  //   'level 7',
];
const activeColor = '#6778D0';

export default function TrackingStatus() {
  const [activeIndex, setActive] = useState(0);

  const setActiveIndex = val => {
    LayoutAnimation.easeInEaseOut();
    setActive(val);
  };
  const marginLeft = (100 / (status.length - 1)) * activeIndex - 150 + '%';
  return (
    <View style={styles.constainer}>
      {/* <Text style={styles.prop}>{activeIndex}</Text> */}
      <View style={styles.statusContainer}>
        <View style={styles.line}>
          <View style={[styles.activeLine, {marginLeft}]} />
        </View>
        {status.map((status, index) => (
          <View style={[styles.dot]} key={status}>
            <View
              style={[
                index <= activeIndex
                  ? {height: '100%', width: '100%'}
                  : {height: '40%', width: '40%'},
                {backgroundColor: activeColor, borderRadius: 20},
              ]}
            />
          </View>
        ))}
        <View style={styles.labelContainer}>
          {status.map((status, index) => (
            <View style={{flexDirection: 'column'}}>
              <Text key={status.id} numberOfLines={1} style={styles.label}>
                {status.level}
              </Text>
              <Text key={status.id} numberOfLines={1} style={styles.label1}>
                {status.xp} XP
              </Text>
            </View>
          ))}
        </View>
      </View>
      {/* <View style={styles.btns}>
        <Button
          title="prev"
          onPress={() => setActiveIndex(activeIndex - 0.5)}
          disabled={activeIndex <= 0}
        />
        <Button
          title="next"
          onPress={() => setActiveIndex(activeIndex + 1)}
          disabled={activeIndex >= status.length - 1}
        />
      </View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  constainer: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    // paddingHorizontal: 30,
    marginLeft: 140,
  },
  statusContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: 70,
    justifyContent: 'space-between',
  },
  dot: {
    height: 15,
    width: 15,
    borderRadius: 10,
    backgroundColor: '#D4D7ED',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    height: 5,
    width: '150%',
    backgroundColor: '#D4D7ED',
    position: 'absolute',
    borderRadius: 5,
    overflow: 'hidden',
  },
  activeLine: {
    height: '100%',
    width: '100%',
    backgroundColor: activeColor,
    borderRadius: 5,
  },
  btns: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 20,
  },
  labelContainer: {
    width: '100%',
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  label: {
    fontSize: 12,
    top: 30,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
  },
  label1: {
    fontSize: 10,
    top: 30,
    textAlign: 'center',
    color: 'grey',
    fontWeight: 'bold',
  },
  prop: {
    marginBottom: 20,
    width: 100,
    textAlign: 'center',
  },
});
