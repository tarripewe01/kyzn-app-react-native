import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {fonts} from '../../../utils/Fonts';

const CardTitle = ({title, onPress}) => {
  return (
    <View style={styles.page}>
      <Text style={styles.title}>{title}</Text>

      <TouchableOpacity onPress={onPress}>
        <Text style={styles.CTA}>View All</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CardTitle;

const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 16,
    paddingRight: 16,
    paddingLeft: 16,
  },
  title: {
    fontFamily: fonts.Rubik[700],
    fontSize: 14,
    letterSpacing: 1,
  },
  CTA: {
    fontFamily: fonts.Rubik[400],
    fontSize: 10,
    letterSpacing: 1,
  },
});
