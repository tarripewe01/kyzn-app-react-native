import React from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {ICScan, ICSearch} from '../../../assets';
import {colors} from '../../../utils/Colors';

const imageLogo = require('../../../assets/Images/images_logo.png');
const imagePremium = require('../../../assets/Images/image_premium.png');

const HeaderHome = ({MyQr, search}) => {
  return (
    <View style={styles.page}>
      <View style={styles.containHeader}>
        <Image source={imageLogo} style={styles.imageLogo} />
        <Image source={imagePremium} style={styles.imagePremium} />
      </View>

      <View style={styles.containIcon}>
        <TouchableOpacity onPress={MyQr} style={styles.iconScan}>
          <ICScan />
        </TouchableOpacity>
        <TouchableOpacity onPress={search}>
          <ICSearch />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HeaderHome;

const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    alignItems: 'center',
    paddingHorizontal: 16,
    backgroundColor: colors.white1,
    paddingTop: 50,
    // marginTop: Platform.OS === 'android' ? -40 : 0,
  },
  containHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageLogo: {
    width: 55,
    height: 20,
    marginRight: 10,
  },
  imagePremium: {width: 90, height: 20, marginTop: 3},
  containIcon: {flexDirection: 'row'},
  iconScan: {marginRight: 13},
});
