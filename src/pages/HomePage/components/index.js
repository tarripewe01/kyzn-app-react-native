import CardCourse from './CardCourse';
import CardProfile from './CardProfile';
import CardPromo from './CardPromo';
import CardTitle from './CardTitle';
import CardUpcoming from './CardUpcoming';
import HeaderHome from './HeaderHome';
import CardPremium from './CardPremium';
import CardTrainer from './CardTrainer';

export {
  CardCourse,
  CardProfile,
  CardPromo,
  CardTitle,
  CardUpcoming,
  HeaderHome,
  CardPremium,
  CardTrainer,
};
