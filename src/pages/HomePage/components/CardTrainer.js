import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors} from '../../../utils/Colors';
import {fonts} from '../../../utils/Fonts';

const imageXp = require('../../../assets/Images/image_xp.png');
const imageStar = require('../../../assets/Images/image_star.png');

const CardTrainer = ({trainer, name, price, xp, rating}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity onPress={() => navigation.navigate('Detail Trainer')}>
      <View style={styles.page}>
        <View>
          <Image source={trainer} style={styles.imageCourse} />
        </View>
        <View style={styles.contain}>
          <Text style={styles.title}>{name}</Text>
          <Text style={styles.price}>Rp {price}</Text>
          <View style={styles.containXp}>
            <View style={styles.containStar}>
              <Image source={imageStar} style={styles.imageXp} />
              <Text style={styles.textXp}>{rating}</Text>
            </View>
            <View style={styles.containerXp}>
              <Image source={imageXp} style={styles.imageXp} />
              <Text style={styles.textXp}>{xp}</Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CardTrainer;

const styles = StyleSheet.create({
  page: {
    width: 140,
    height: 220,
    borderWidth: 1,
    borderColor: colors.grey4,
    borderRadius: 5,
    shadowColor: '#000',
    marginRight: -10,
    marginLeft: 16,
  },
  imageCourse: {width: 140, height: 130},
  contain: {paddingLeft: 8, marginTop: 11},
  title: {fontFamily: fonts.Rubik[500], fontSize: 11},
  price: {
    fontFamily: fonts.Rubik[700],
    fontSize: 12,
    marginTop: 10,
  },
  containXp: {flexDirection: 'row', alignItems: 'center'},
  imageXp: {width: 15, height: 15, marginRight: 5, marginTop: 4},
  textXp: {
    fontFamily: fonts.Rubik[500],
    fontSize: 12,
    marginTop: 6,
  },
  containStar: {flexDirection: 'row', marginRight: 5},
  containerXp: {flexDirection: 'row'},
});
