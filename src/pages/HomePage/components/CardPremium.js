import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {colors} from '../../../utils/Colors';
import {fonts} from '../../../utils/Fonts';

const CardPremium = ({imageBg, price, pass}) => {
  return (
    <View style={styles.page}>
      <ImageBackground source={imageBg} style={styles.imageBg}>
        <View style={styles.containTitle}>
          <Text style={styles.price}>Rp {price}</Text>
          <Text style={styles.pass}>{pass} Months Platinum Pass</Text>
        </View>
      </ImageBackground>
    </View>
  );
};

export default CardPremium;

const styles = StyleSheet.create({
  page: {marginRight: -5, marginLeft: 16},
  imageBg: {width: 190, height: 262},
  containTitle: {
    marginTop: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  price: {
    fontFamily: fonts.Rubik[700],
    fontSize: 14,
    color: colors.white1,
  },
  pass: {
    fontFamily: fonts.Rubik[500],
    fontSize: 12,
    color: colors.white1,
    marginTop: 4,
  },
});
