import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors} from '../../../utils/Colors';
import {fonts} from '../../../utils/Fonts';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const changeTitle = () => {
  if (type === 'Move') {
    return colors.purple1;
  }
  if (type === 'Stand') {
    return colors.red1;
  }
  if (type === 'Step') {
    return colors.yellow1;
  }
  if (type === 'Exercise') {
    return colors.green1;
  }
};

const CardProfile = ({
  value,
  maxValue,
  size,
  title,
  subTitle,
  persen,
  type,
  image,
}) => {
  return (
    <View style={styles.page}>
      <View style={styles.containIndicator}>
        <Image source={image} style={styles.imageIndicator} />
        <View style={styles.containContent}>
          <View style={styles.content}>
            <Text style={styles.title(type)}>{title}</Text>
            <Text style={styles.textPersen}>{persen}</Text>
          </View>
          <Text style={styles.textCal}>{subTitle}</Text>
        </View>
      </View>
    </View>
  );
};

export default CardProfile;

const styles = StyleSheet.create({
  page: {
    backgroundColor: 'white',
    marginHorizontal: 13,
    width: 130,
    height: 45,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: colors.grey4,
  },
  containIndicator: {
    flexDirection: 'row',
    paddingLeft: 5,
    alignItems: 'center',
    paddingVertical: 7,
  },
  imageIndicator: {width: 24, height: 24, marginRight: 6},
  containContent: {flexDirection: 'column'},
  content: {flexDirection: 'row'},
  title: type => ({
    fontFamily: fonts.Rubik[700],
    fontSize: 10,
    color: type === 'Move' ? colors.purple1 : colors.red1,
    marginRight: 2,
  }),
  textPersen: {
    fontFamily: fonts.Rubik[500],
    fontSize: 10,
    color: colors.purple2,
  },
  textCal: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 8,
    marginTop: 2,
  },
});
