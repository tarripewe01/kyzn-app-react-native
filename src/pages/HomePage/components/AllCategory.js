import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  ImageBackground,
  Text,
} from 'react-native';
import {
  ICArrowNext2,
  ICCourts,
  ICEducations,
  ICFitness,
  ICNutritions,
  ICSports,
  ICWellness,
} from '../../../assets';
import {colors, fonts} from '../../../utils';

const imageBg = require('../../../assets/Images/image_cardActivity.png');

const CardCategory = ({title, icon, onPress}) => {
  return (
    <View style={{paddingLeft: 16, marginBottom: 20}}>
      <TouchableOpacity onPress={onPress}>
        <ImageBackground source={imageBg} style={styles.card}>
          <View style={styles.contentCard}>
            {icon}
            <Text style={styles.textCard}>{title}</Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

const AllCategory = () => {
  return (
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
      <View style={{flexDirection: 'row'}}>
        <View>
          <CardCategory
            title="Wellness"
            icon={<ICWellness />}
            onPress={() => navigation.navigate('Wellness Category')}
          />
          <CardCategory
            title="Fitness"
            icon={<ICFitness />}
            onPress={() => navigation.navigate('Fitness Category')}
          />
          <CardCategory title="Educations" icon={<ICEducations />} />
        </View>
        <View>
          <CardCategory
            title="Sports"
            icon={<ICSports />}
            onPress={() => navigation.navigate('Sports Category')}
          />
          <CardCategory title="Book Courts" icon={<ICCourts />} />
          <CardCategory title="Nutritions" icon={<ICNutritions />} />
        </View>
      </View>
      <TouchableOpacity>
        <View style={styles.containNext}>
          <View style={styles.iconNext}>
            <ICArrowNext2 />
          </View>
        </View>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default AllCategory;

const styles = StyleSheet.create({
  card: {
    width: 160,
    height: 42,
    marginRight: -5,
  },
  contentCard: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 16,
    paddingVertical: 5,
  },
  textCard: {
    fontFamily: fonts.Rubik[500],
    marginLeft: 8,
    fontSize: 11,
    color: colors.grey8,
  },
  containNext: {
    width: 34,
    height: 165,
    backgroundColor: colors.purple3,
    marginLeft: 20,
  },
  iconNext: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 165,
  },
});
