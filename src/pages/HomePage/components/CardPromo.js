import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {fonts} from '../../../utils/Fonts';

const imagePromo = require('../../../assets/Images/image_hero_promo.png');

const CardPromo = () => {
  return (
    <>
      <View style={styles.containTitle}>
        <Text style={styles.textPromo}>Info & Promo</Text>
        <TouchableOpacity>
          <Text style={styles.CTA}>View All</Text>
        </TouchableOpacity>
      </View>
      <View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <Image source={imagePromo} style={styles.imagePromo} />
          <Image source={imagePromo} style={styles.imagePromo} />
          <Image source={imagePromo} style={styles.imagePromo} />
        </ScrollView>
      </View>
    </>
  );
};

export default CardPromo;

const styles = StyleSheet.create({
  containTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 16,
  },
  textPromo: {
    fontFamily: fonts.Rubik[700],
    fontSize: 14,
    letterSpacing: 1.5,
    paddingLeft: 16,
  },
  CTA: {
    fontFamily: fonts.Rubik[400],
    fontSize: 10,
    letterSpacing: 1,
    paddingRight: 16,
  },
  imagePromo: {width: 328, height: 100, marginLeft: 16},
});
