import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  ICArrowNext2,
  ICCourts,
  ICEducations,
  ICFitness,
  ICNutritions,
  ICSports,
  ICWellness,
} from '../../assets';
import {Gap} from '../../components';
import {colors} from '../../utils/Colors';
import {fonts} from '../../utils/Fonts';
import {
  CardCourse,
  CardPremium,
  CardProfile,
  CardPromo,
  CardTitle,
  CardTrainer,
  CardUpcoming,
  HeaderHome,
} from './components';
import TrackingStatus from './components/TrackingStatus';

const bgLeft = require('../../assets/Images/image_left.png');
const bgRight = require('../../assets/Images/image_right.png');
const xp = require('../../assets/Images/image_xp.png');
const coin = require('../../assets/Images/image_coin.png');
const move = require('../../assets/Dummy/image_indicatorMove.png');
const stand = require('../../assets/Dummy/image_indicatorStand.png');
const step = require('../../assets/Dummy/image_indicatorStep.png');
const exercise = require('../../assets/Dummy/image_indicatorExercise.png');
const course1 = require('../../assets/Dummy/dummy_course1.png');
const course2 = require('../../assets/Dummy/dummy_course2.png');
const course3 = require('../../assets/Dummy/dummy_course3.png');
const course4 = require('../../assets/Dummy/dummy_course4.png');
const course5 = require('../../assets/Dummy/dummy_course5.png');
const premium1 = require('../../assets/Images/images_premium1.png');
const premium2 = require('../../assets/Images/images_premium2.png');
const premium3 = require('../../assets/Images/images_premium3.png');
const trainer1 = require('../../assets/Dummy/dummy_trainer1.png');
const trainer2 = require('../../assets/Dummy/dummy_trainer2.png');
const trainer3 = require('../../assets/Dummy/dummy_trainer3.png');
const fitness = require('../../assets/Dummy/dummy_fitness.png');
const video1 = require('../../assets/Dummy/dummy_video1.png');
const video2 = require('../../assets/Dummy/dummy_video2.png');
const video3 = require('../../assets/Dummy/dummy_video3.png');
const imageBg = require('../../assets/Images/image_cardActivity.png');
const dummylevel = require('../../assets/Dummy/dummy_level.png');
const dummyblack = require('../../assets/Dummy/dummy_black.png');
const dummyprogress = require('../../assets/Dummy/dummy_progressbar.png');
const dummyProfile = require('../../assets/Dummy/image_profile.png');

const CardCategory = ({title, icon, onPress}) => {
  return (
    <View style={{paddingLeft: 16, marginBottom: 20}}>
      <TouchableOpacity onPress={onPress}>
        <ImageBackground source={imageBg} style={styles.card}>
          <View style={styles.contentCard}>
            {icon}
            <Text style={styles.textCard}>{title}</Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

const HomePage = ({navigation}) => {
  // const navigation = useNavigation();
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.page}>
        <StatusBar
          translucent={true}
          backgroundColor="transparent"
          barStyle="dark-content"
        />

        <HeaderHome
          MyQr={() => navigation.navigate('My QR')}
          search={() => navigation.navigate('search')}
        />

        <View style={{backgroundColor: colors.white1}}>
          <ImageBackground source={bgLeft} style={styles.bg}>
            <ImageBackground source={bgRight} style={styles.bg}>
              <View style={styles.contain}>
                <View>
                  <Gap height={75} />
                  <CardProfile
                    image={move}
                    title="Move"
                    type="Move"
                    subTitle="625/550 Cal"
                    persen="113%"
                  />
                  <Gap height={20} />
                  <CardProfile
                    image={stand}
                    title="Stand"
                    type="Stand"
                    subTitle="12/12 HR"
                    persen="100%"
                  />
                </View>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Loyalty Program')}>
                  <View style={{marginTop: 85}}>
                    <ImageBackground
                      source={dummyProfile}
                      style={{width: 80, height: 80}}>
                      <Image
                        source={dummylevel}
                        style={{marginLeft: 18, marginTop: 65}}
                      />
                    </ImageBackground>
                  </View>
                </TouchableOpacity>

                <View>
                  <Gap height={75} />
                  <CardProfile
                    image={step}
                    title="Step"
                    type="Step"
                    subTitle="12/12 HR"
                    persen="113%"
                  />
                  <Gap height={20} />
                  <CardProfile
                    image={exercise}
                    title="Exercise"
                    type="Exercise"
                    subTitle="50/25 Min"
                    persen="113%"
                  />
                </View>
              </View>
            </ImageBackground>
          </ImageBackground>
        </View>

        <Gap height={68} />
        <View style={styles.containInfo}>
          <Text style={styles.title}>Riana Permatasari</Text>
          <View style={styles.containReward}>
            <View style={styles.rewardXP}>
              <Image source={xp} style={styles.imageReward} />
              <Text style={styles.reward}>800 XP</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image source={coin} style={styles.imageReward} />
              <Text style={styles.reward}>42 Coin</Text>
            </View>
          </View>
        </View>

        <Gap height={25} />
        <View style={{alignItems: 'center', }}>
          <TrackingStatus />
        </View>

        <Gap height={30} />

        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <CardCategory
                title="Wellness"
                icon={<ICWellness />}
                onPress={() => navigation.navigate('Wellness Category')}
              />
              <CardCategory
                title="Fitness"
                icon={<ICFitness />}
                onPress={() => navigation.navigate('Fitness Category')}
              />
              <CardCategory title="Educations" icon={<ICEducations />} />
            </View>
            <View>
              <CardCategory
                title="Sports"
                icon={<ICSports />}
                onPress={() => navigation.navigate('Sports Category')}
              />
              <CardCategory title="Book Courts" icon={<ICCourts />} />
              <CardCategory title="Nutritions" icon={<ICNutritions />} />
            </View>
          </View>
          <TouchableOpacity>
            <View style={styles.containNext}>
              <View style={styles.iconNext}>
                <ICArrowNext2 />
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>

        <Gap height={32} />
        <View style={styles.containText}>
          <Text style={styles.textUpcoming}>Upcoming Activity</Text>
        </View>

        <Gap height={16} />
        <>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <CardUpcoming />
              <CardUpcoming />
              <CardUpcoming />
            </View>
          </ScrollView>
        </>

        <Gap height={32} />
        <CardPromo />

        <Gap height={32} />
        <View>
          <CardTitle
            title="Available Schedule"
            onPress={() => navigation.navigate('Available Schedule')}
          />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <CardCourse
                course={course1}
                title="Yoga for Beginner"
                price="299.000"
                xp="800"
              />
              <CardCourse
                course={course2}
                title="Boxing"
                price="299.000"
                xp="800"
              />
              <CardCourse
                course={course3}
                title="Yoga Advance"
                price="299.000"
                xp="800"
              />
              <CardCourse
                course={course4}
                title="Yoga Advance"
                price="299.000"
                xp="800"
              />
            </View>
          </ScrollView>
        </View>

        <Gap height={32} />
        <View>
          <CardTitle
            title="KYZN Premium"
            onPress={() => navigation.navigate('Premium Membership')}
          />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <CardPremium imageBg={premium1} price="499.900" pass="3" />
              <CardPremium imageBg={premium2} price="2.999.900" pass="6" />
              <CardPremium imageBg={premium3} price="5.999.900" pass="12" />
            </View>
          </ScrollView>
        </View>

        <Gap height={32} />
        <View>
          <CardTitle
            title="Personal Trainer"
            onPress={() => navigation.navigate('Trainer')}
          />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <CardTrainer
                trainer={trainer1}
                name="Shameer Jainab"
                price="299.000"
                rating="4.7"
                xp="800"
              />
              <CardTrainer
                trainer={trainer2}
                name="Khazan Jacob"
                price="299.000"
                rating="4.7"
                xp="800"
              />
              <CardTrainer
                trainer={trainer3}
                name="Shameer Jainab"
                price="299.000"
                rating="4.7"
                xp="800"
              />
              <CardTrainer
                trainer={trainer2}
                name="Khazan Jacob"
                price="299.000"
                rating="4.7"
                xp="800"
              />
            </View>
          </ScrollView>
        </View>

        <Gap height={32} />
        <View>
          <CardTitle
            title="Wellness"
            onPress={() => navigation.navigate('Wellness')}
          />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <CardCourse
                course={course1}
                title="Yoga for Beginner"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Class')}
              />
              <CardCourse
                course={course2}
                title="Boxing"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Class')}
              />
              <CardCourse
                course={course3}
                title="Yoga Advance"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Class')}
              />
              <CardCourse
                course={course4}
                title="Yoga Advance"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Class')}
              />
            </View>
          </ScrollView>
        </View>

        <Gap height={32} />
        <View style={styles.container}>
          <CardTitle
            title="Sports"
            onPress={() => navigation.navigate('Sports')}
          />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <CardCourse
                course={course4}
                title="Yoga Advance"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Sport')}
              />
              <CardCourse
                course={course2}
                title="Boxing"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Sport')}
              />
              <CardCourse
                course={course3}
                title="Yoga Advance"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Sport')}
              />
              <CardCourse
                course={course5}
                title="Yoga Advance"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Sport')}
              />
            </View>
          </ScrollView>
        </View>

        <Gap height={32} />
        <View>
          <CardTitle
            title="Fitness"
            onPress={() => navigation.navigate('Fitness')}
          />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <CardCourse
                course={fitness}
                title="Strength & Condition"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Fitness')}
              />
              <CardCourse
                course={fitness}
                title="Body Pump "
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Fitness')}
              />
              <CardCourse
                course={fitness}
                title="Body Pump "
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Fitness')}
              />
              <CardCourse
                course={fitness}
                title="Body Pump "
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Detail Fitness')}
              />
            </View>
          </ScrollView>
        </View>

        <Gap height={32} />
        <View style={styles.container}>
          <CardTitle
            title="Workout Video"
            onPress={() => navigation.navigate('Workout Video')}
          />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <CardCourse
                course={video1}
                title="Yoga for Beginner"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Purchase Video')}
              />
              <CardCourse
                course={video2}
                title="Boxing"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Purchase Video')}
              />
              <CardCourse
                course={video3}
                title="Yoga Advance"
                price="299.000"
                xp="800"
                onPress={() => navigation.navigate('Purchase Video')}
              />
            </View>
          </ScrollView>
        </View>
        <Gap height={32} />
      </View>
    </ScrollView>
  );
};

export default HomePage;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
  bg: {width: '100%', height: 120, marginTop: -20},
  contain: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: -10,
  },
  containInfo: {alignItems: 'center'},
  title: {fontFamily: fonts.Rubik[700], fontSize: 16, lineHeight: 18},
  containReward: {flexDirection: 'row', marginTop: 10},
  rewardXP: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 30,
  },
  imageReward: {width: 24, height: 24},
  reward: {fontFamily: fonts.Rubik[500], fontSize: 13, marginLeft: 5},
  containCard: {paddingHorizontal: 15, marginBottom: 20, marginRight: -20},
  card: {
    width: 160,
    height: 42,
    marginRight: -5,
  },
  contentCard: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 16,
    paddingVertical: 5,
  },
  textCard: {
    fontFamily: fonts.Rubik[500],
    marginLeft: 8,
    fontSize: 11,
    color: colors.grey8,
  },
  containText: {paddingHorizontal: 16},
  textUpcoming: {
    fontFamily: fonts.Rubik[700],
    fontSize: 14,
    letterSpacing: 1.5,
  },
  // container: {paddingLeft: 16},
  containNext: {
    width: 34,
    height: 165,
    backgroundColor: colors.purple3,
    marginLeft: 20,
  },
  iconNext: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 165,
  },
});
