import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {WellnessCategoryPage} from '..';
import {ICArrowBackWhite, ICLoyaltyProgram} from '../../assets';
import {Gap} from '../../components';
import {colors, fonts} from '../../utils';
import CourtsCategory from '../LoyaltyProgramPage/category/courts';
import DrinksCategory from '../LoyaltyProgramPage/category/drinks';
import FoodsCategory from '../LoyaltyProgramPage/category/foods';
import SportsCategory from '../LoyaltyProgramPage/category/sports';

const Tab = createMaterialTopTabNavigator();

const DetailLoyaltyPrograms = () => {
  const navigation = useNavigation();
  return (
    <>
      <StatusBar barStyle="light-content" />
      <ScrollView
        style={{flex: 1, backgroundColor: colors.white2}}
        showsVerticalScrollIndicator={false}>
        <ImageBackground
          style={{height: Dimensions.get('window').height / 2.5}}
          source={require('../../assets/Images/images_headerloyalty.png')}>
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <TouchableOpacity
              onPress={() => navigation.goBack('Account')}
              style={{marginTop: 30, paddingHorizontal: 20}}>
              <ICArrowBackWhite />
            </TouchableOpacity>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                flex: 1,
                paddingRight: 50,
                marginTop: 30,
              }}>
              <ICLoyaltyProgram />
            </View>
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 15,
            }}>
            <Text
              style={{
                fontFamily: fonts.Montserrat[700],
                fontSize: 14,
                color: colors.white2,
              }}>
              KYZN Loyalty Program
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
            }}>
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Image
                style={{width: 24, height: 24}}
                source={require('../../assets/Images/image_xp.png')}
              />
              <Text
                style={{
                  fontFamily: fonts.Montserrat[700],
                  fontSize: 12,
                  color: colors.white2,
                  marginLeft: 5,
                }}>
                800 XP
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 24, height: 24}}
                source={require('../../assets/Images/image_coin.png')}
              />
              <Text
                style={{
                  fontFamily: fonts.Montserrat[700],
                  fontSize: 12,
                  color: colors.white2,
                  marginLeft: 5,
                }}>
                42 Coin
              </Text>
            </View>
          </View>

          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <View
              style={{
                width: 55,
                height: 20,
                backgroundColor: colors.white2,
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 20,
                borderRadius: 3,
                marginTop: 15,
              }}>
              <Text
                style={{
                  fontFamily: fonts.Montserrat[700],
                  fontSize: 11,
                  color: colors.purple1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                Level 1
              </Text>
            </View>
          </View>
        </ImageBackground>

        <View
          style={{
            flex: 1.5,
            backgroundColor: colors.white2,
            bottom: 50,
            borderTopStartRadius: 30,
            borderTopEndRadius: 30,
          }}>
          <View style={{padding: 40}}>
            <Image
              style={{width: 370}}
              source={require('../../assets/Dummy/dummy_progressbar.png')}
            />
          </View>

          <View style={{paddingHorizontal: 20}}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 14}}>
                Zeni History
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('Zeni History')}>
                <Text style={{fontFamily: fonts.Montserrat[500], fontSize: 10}}>
                  View All
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              borderWidth: 1,
              borderColor: colors.grey6,
              borderRadius: 5,
              marginTop: 12,
              height: 63,
              width: 350,
              paddingVertical: 10,
              paddingHorizontal: 16,
              marginHorizontal: 20,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View>
                <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 12}}>
                  Happy Birthday Claim
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.Montserrat[600],
                    fontSize: 10,
                    color: colors.grey8,
                  }}>
                  24 Jun 2021
                </Text>
              </View>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Text
                  style={{
                    fontFamily: fonts.Montserrat[700],
                    fontSize: 12,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  +42
                </Text>
              </View>
            </View>
          </View>
          <Gap height={32} />
          <View style={{height: 5, backgroundColor: colors.grey4}}></View>
          <View
            style={{
              paddingHorizontal: 20,
              paddingTop: 32,
            }}>
            <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 14}}>
              ZENI Shop
            </Text>
            <Text
              style={{
                fontFamily: fonts.Montserrat[500],
                fontSize: 10,
                textAlign: 'justify',
                marginTop: 15,
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore.
            </Text>
          </View>

          <Tab.Navigator
            screenOptions={{
              tabBarLabelStyle: {
                fontSize: 12,
                fontFamily: fonts.Montserrat[700],
              },
              tabBarStyle: {width: '100%', paddingTop: 10, marginTop: -20},
              tabBarAllowFontScaling: true,
              tabBarLabelStyle: {textTransform: 'none'},
              tabBarIndicatorStyle: {backgroundColor: colors.black1},
              tabBarItemStyle: {
                width: 100,
                paddingHorizontal: -10,
                paddingVertical: -10,
              },
              tabBarScrollEnabled: true,
            }}>
            <Tab.Screen name="Sports" component={SportsCategory} />
            <Tab.Screen name="Wellness" component={WellnessCategoryPage} />
            <Tab.Screen name="Courts" component={CourtsCategory} />
            <Tab.Screen name="Foods" component={FoodsCategory} />
            <Tab.Screen name="Drinks" component={DrinksCategory} />
          </Tab.Navigator>
          <Gap height={300} />
        </View>
      </ScrollView>
    </>
  );
};

export default DetailLoyaltyPrograms;

const styles = StyleSheet.create({});
