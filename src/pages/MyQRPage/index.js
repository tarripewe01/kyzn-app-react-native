import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ICClose, ICQrWhite} from '../../assets';
import {Bar, Gap} from '../../components';
import {colors, fonts} from '../../utils';

const MyQRPage = () => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: colors.white2}}>
      <Bar />

      <ImageBackground
        style={{width: '100%', height: '100%'}}
        source={require('../../assets/Images/image_bgQR.png')}>
        <View style={{marginTop: 50, paddingHorizontal: 20}}>
          <TouchableOpacity onPress={() => navigation.goBack('Account')}>
            <ICClose />
          </TouchableOpacity>
        </View>
        <Gap height={85} />
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Image source={require('../../assets/Images/image_QR.png')} />
          <Text
            style={{
              fontFamily: fonts.Montserrat[700],
              fontSize: 16,
              marginTop: 15,
            }}>
            Paul McGabee
          </Text>
        </View>

        <Gap height={350} />
        <View
          style={{
            backgroundColor: colors.black2,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            width: 350,
            marginHorizontal: 20,
            borderRadius: 5,
          }}>
          <ICQrWhite />
          <Text
            style={{
              fontFamily: fonts.Montserrat[700],
              fontSize: 14,
              color: colors.white2,
              marginLeft: 15,
            }}>
            Scan QR Code
          </Text>
        </View>
      </ImageBackground>
    </View>
  );
};

export default MyQRPage;

const styles = StyleSheet.create({});
