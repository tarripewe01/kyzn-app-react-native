import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {FAB} from 'react-native-paper';
import {ICBurger} from '../../assets';
import {Bar, Divider, Gap, HeaderTitle} from '../../components';
import {fonts} from '../../utils';
import {colors} from '../../utils/Colors';
import Inbox from './components/inbox';

const InboxPage = () => {
  return (
    <>
      <Bar />
      <ScrollView showsVerticalScrollIndicator={false} style={styles.page}>
        <View style={{marginTop: 20}}>
          <HeaderTitle title="Messages" icon={<ICBurger />} />

          <Gap height={20} />
          <Inbox
            title="Happy Birthday Riana"
            date="23 Jan 2021"
            text="Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj..."
            type="unread"
            margin="Birthday"
          />
          <Divider />
          <Inbox
            title="KYZN Promo"
            date="23 Jan 2021"
            text="Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj..."
            type="read"
            margin=""
          />
          <Divider />
          <Inbox
            title="KYZN Promo"
            date="23 Jan 2021"
            text="Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj..."
            type="unread"
            margin=""
          />
          <Divider />
          <Inbox
            title="KYZN Promo"
            date="23 Jan 2021"
            text="Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj..."
            type="read"
            margin=""
          />
          <Divider />
          <Inbox
            title="KYZN Promo"
            date="23 Jan 2021"
            text="Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj..."
            type="read"
            margin=""
          />
          <Inbox
            title="KYZN Promo"
            date="23 Jan 2021"
            text="Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj..."
            type="unread"
            margin=""
          />
          <Divider />

          <Gap height={50} />

          <TouchableOpacity>
            <View style={styles.containButton}>
              <View style={styles.button}>
                <Text style={styles.textButton}>Claim All</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
};

export default InboxPage;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  fab: {
    position: 'absolute',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: colors.blue1,
    height: 24,
    width: 120,
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: fonts.Montserrat[600],
    margin: 30,
    right: '30%',
    bottom: 0,
  },
  containButton: {
    width: '100%',
    alignItems: 'center',
    marginTop: Platform.OS === 'android' ? -20 : 20,
    marginBottom: Platform.OS === 'android' ? 20 : 0,
  },
  button: {
    width: 110,
    height: 30,
    backgroundColor: colors.blue1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  textButton: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    color: colors.white2,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
