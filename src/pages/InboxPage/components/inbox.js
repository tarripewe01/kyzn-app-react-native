import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ICGift} from '../../../assets';
import {fonts} from '../../../utils';
import {colors} from '../../../utils/Colors';

const imgRound = require('../../../assets/Dummy/dummy_round.png');

const Inbox = ({title, date, text, type, margin}) => {
  const navigation = useNavigation();
  return (
    <>
      <TouchableOpacity onPress={() => navigation.navigate('Detail Inbox')}>
        <View style={styles.page(type)}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.title}>{title}</Text>
            {title === 'Happy Birthday Riana' ? (
              <ImageBackground source={imgRound} style={styles.imageBg}>
                <View style={styles.icon}>
                  <ICGift />
                </View>
              </ImageBackground>
            ) : null}
          </View>
          <Text style={styles.date(margin)}>{date}</Text>

          <Text style={styles.text}>{text}</Text>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default Inbox;

const styles = StyleSheet.create({
  page: type => ({
    paddingLeft: 20,
    backgroundColor: type === 'unread' ? colors.blue3 : colors.white2,
  }),
  title: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    letterSpacing: 0.8,
    marginTop: 16,
  },
  imageBg: {width: 56, height: 60},
  icon: {marginLeft: 22, marginTop: 10},
  date: margin => ({
    fontFamily: fonts.Montserrat[500],
    fontSize: 10,
    marginTop: margin === 'Birthday' ? -15 : 5,
  }),
  text: {
    fontFamily: fonts.Montserrat[400],
    fontSize: 10,
    marginTop: 5,
    marginBottom: 10,
    textAlign: 'justify',
    marginRight: 20,
  },
});
