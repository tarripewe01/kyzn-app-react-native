import {useNavigation} from '@react-navigation/native';
import React, {useRef, useState} from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ListItem} from 'react-native-elements';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  ICCourts,
  ICFitness1,
  ICFitness2,
  ICFitness3,
  ICFitness4,
  ICFitness5,
  ICSports,
  ICWellness,
} from '../../assets';
import {Bar, Gap} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../utils';
import SelectServices from '../WellnessCategoryPage/SelectServices';

const Category = [
  {
    id: 1,
    icon: <ICFitness1 />,
    title: 'Forge Your Strength!',
    Subtitle:
      'If you are looking to build some muscle, you are on the right path! Burn some fat, and get your heart rate up! Believe us, you will thank yourself at the end of it!	',
  },
  {
    id: 2,
    icon: <ICFitness2 />,
    title: 'Break a Sweat!',
    Subtitle:
      "If you are looking to burn some calories, this is the path for you! Get that adrenalin pumping and burn, burn, burn! Don't worry about how, becuase your sweat will speak for itself!		",
  },
  {
    id: 3,
    icon: <ICFitness3 />,
    title: 'Just do it!',
    Subtitle:
      "Short on time? If you are looking for a quick boost of energy, a quick sweat, or a quick reason to feel fabulous about yourself, look no more! Don't forget, something is always better than nothing! 		",
  },
  {
    id: 4,
    icon: <ICFitness4 />,
    title: 'Shake it! ',
    Subtitle:
      "5,6,7,8, it's time to get you really fit! With zero experience necessary, show up to any of these classes to simply shake it off. ",
  },
  {
    id: 5,
    icon: <ICFitness5 />,
    title: 'Let it Flow!',
    Subtitle:
      'Just breathe in, breathe out, and let it all go. Trust the process, and pick your own flow.		',
  },
];

const FitnessCategoryPage = () => {
  const navigation = useNavigation();

  const [visible, setVisible] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const refRBSheet = useRef();

  const handleBottomSheet = () => refRBSheet.current.open();

  const handleGroupClass = () => {
    navigation.navigate('Fitness'), refRBSheet.current.close();
  };

  return (
    <View style={styles.page}>
      <Bar />
      <View>
        <HeaderCourse
          title="Select Fitness"
          onPress={() => navigation.navigate('MainApp')}
        />
      </View>

      <Gap height={30} />
      {Category.map((item, i) => (
        <TouchableOpacity onPress={handleBottomSheet}>
          <RBSheet
            height={250}
            ref={refRBSheet}
            closeOnDragDown={true}
            closeOnPressMask={true}>
            <View>
              <View style={styles.containText}>
                <Text style={styles.text}>Select Services</Text>
              </View>

              <SelectServices
                icon={<ICWellness />}
                title="Group Class"
                onPress={handleGroupClass}
              />
            </View>
          </RBSheet>

          <ListItem key={item.id} style={styles.containList}>
            <View style={styles.icon}>{item.icon}</View>
            <ListItem.Content>
              <ListItem.Title style={styles.title}>{item.title}</ListItem.Title>
              <ListItem.Subtitle style={styles.subtitle}>
                {item.Subtitle}
              </ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default FitnessCategoryPage;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  containText: {
    paddingHorizontal: 20,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {fontSize: 18, fontWeight: '700'},
  containList: {paddingHorizontal: 20, marginBottom: 5},
  icon: {width: 40, height: 40, alignItems: 'center'},
  title: {fontFamily: fonts.Montserrat[700], fontSize: 14},
  subtitle: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 10,
    marginTop: 5,
  },
});
