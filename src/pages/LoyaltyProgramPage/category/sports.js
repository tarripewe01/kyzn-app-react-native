import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { colors } from '../../../utils';

const SportsCategory = () => {
  return (
    <View style={{flex: 1, backgroundColor: colors.white2}}>
      <View style={{width: 158, height: 155}}>
        <Image
          style={{width: 158}}
          source={require('../../../assets/Dummy/dummy_sport1.png')}
        />
      </View>
    </View>
  );
};

export default SportsCategory;

const styles = StyleSheet.create({});
