import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  Image,
  ImageBackground,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {WellnessCategoryPage} from '..';
import {ICArrowBackWhite, ICLoyaltyProgram} from '../../assets';
import {Gap} from '../../components';
import {colors, fonts} from '../../utils';
import CourtsCategory from './category/courts';
import DrinksCategory from './category/drinks';
import FoodsCategory from './category/foods';
import SportsCategory from './category/sports';

const Tab = createMaterialTopTabNavigator();

const LoyaltyProgramPage = () => {
  const navigation = useNavigation();
  return (
    <>
      <View style={{flex: 2}}>
        <StatusBar
          barStyle='light-content'
          backgroundColor="transparent"
          translucent={true}
        />
        <ImageBackground
          style={{width: '100%', height: 400}}
          source={require('../../assets/Images/images_headerloyalty.png')}>
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <TouchableOpacity
              onPress={() => navigation.goBack('Account')}
              style={{marginTop: 30, paddingHorizontal: 20}}>
              <ICArrowBackWhite />
            </TouchableOpacity>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                flex: 1,
                paddingRight: 50,
                marginTop: 30,
              }}>
              <ICLoyaltyProgram />
            </View>
          </View>

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 15,
            }}>
            <Text
              style={{
                fontFamily: fonts.Montserrat[700],
                fontSize: 14,
                color: colors.white2,
              }}>
              KYZN Loyalty Program
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
            }}>
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Image
                style={{width: 24, height: 24}}
                source={require('../../assets/Images/image_xp.png')}
              />
              <Text
                style={{
                  fontFamily: fonts.Montserrat[700],
                  fontSize: 12,
                  color: colors.white2,
                  marginLeft: 5,
                }}>
                800 XP
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 24, height: 24}}
                source={require('../../assets/Images/image_coin.png')}
              />
              <Text
                style={{
                  fontFamily: fonts.Montserrat[700],
                  fontSize: 12,
                  color: colors.white2,
                  marginLeft: 5,
                }}>
                42 Coin
              </Text>
            </View>
          </View>

          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <View
              style={{
                width: 55,
                height: 20,
                backgroundColor: colors.white2,
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 20,
                borderRadius: 3,
                marginTop: 15,
              }}>
              <Text
                style={{
                  fontFamily: fonts.Montserrat[700],
                  fontSize: 11,
                  color: colors.purple1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                Level 1
              </Text>
            </View>
          </View>
        </ImageBackground>

        <View
          style={{
            backgroundColor: colors.white2,
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
            paddingTop: 40,
            paddingLeft: 25,
          }}>
          <View>
            <Image
              style={{width: 370}}
              source={require('../../assets/Dummy/dummy_progressbar.png')}
            />
          </View>
          <Gap height={50} />
        </View>

        <View style={{height: 5, backgroundColor: colors.grey4}}></View>
        <View
          style={{
            backgroundColor: colors.white2,
            paddingHorizontal: 20,
            paddingTop: 24,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 14}}>
              Zeni History
            </Text>
            <TouchableOpacity
              style={{alignItems: 'center', justifyContent: 'center'}}>
              <Text style={{fontFamily: fonts.Montserrat[600], fontSize: 10}}>
                View All
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              borderWidth: 1,
              borderColor: colors.grey6,
              borderRadius: 5,
              marginTop: 12,
              height: 63,
              paddingVertical: 17,
              paddingHorizontal: 16,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View>
                <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 12}}>
                  Happy Birthday Claim
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.Montserrat[600],
                    fontSize: 10,
                    color: colors.grey8,
                  }}>
                  24 Jun 2021
                </Text>
              </View>
              <View>
                <Text
                  style={{
                    fontFamily: fonts.Montserrat[700],
                    fontSize: 12,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  +42
                </Text>
              </View>
            </View>
          </View>
          <Gap height={32} />
        </View>

        <View style={{height: 5, backgroundColor: colors.grey4}}></View>
        <View
          style={{
            backgroundColor: colors.white2,
            paddingHorizontal: 20,
            paddingTop: 32,
          }}>
          <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 14}}>
            ZENI Shop
          </Text>
          <Text
            style={{
              fontFamily: fonts.Montserrat[500],
              fontSize: 10,
              textAlign: 'justify',
              marginTop: 15,
            }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore.
          </Text>

          <Gap height={50} />
        </View>
        <Tab.Navigator
          screenOptions={{
            tabBarLabelStyle: {
              fontSize: 12,
              fontFamily: fonts.Montserrat[700],
            },
            tabBarStyle: {width: '100%', paddingTop: 10, marginTop: 300},
            tabBarAllowFontScaling: true,
            tabBarLabelStyle: {textTransform: 'none'},
            tabBarIndicatorStyle: {backgroundColor: colors.black1},
            tabBarItemStyle: {
              width: 100,
              paddingHorizontal: -10,
              paddingVertical: -10,
            },
            tabBarScrollEnabled: true,
          }}>
          <Tab.Screen name="Sports" component={SportsCategory} />
          <Tab.Screen name="Wellness" component={WellnessCategoryPage} />
          <Tab.Screen name="Courts" component={CourtsCategory} />
          <Tab.Screen name="Foods" component={FoodsCategory} />
          <Tab.Screen name="Drinks" component={DrinksCategory} />
        </Tab.Navigator>
      </View>
    </>
  );
};

export default LoyaltyProgramPage;

const styles = StyleSheet.create({});
