import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Bar } from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import { colors } from '../../utils/Colors';

const WellnessGroup = () => {
  return (
    <View style={{flex: 1, backgroundColor: colors.white2}}>
      <Bar />
      <View style={{marginTop: -30}}>
        <HeaderCourse title="Group Classes" />
      </View>

      <Text>Wellness Group</Text>
    </View>
  );
};

export default WellnessGroup;

const styles = StyleSheet.create({});
