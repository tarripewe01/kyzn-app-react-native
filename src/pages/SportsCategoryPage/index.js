import {useNavigation} from '@react-navigation/native';
import React, {useRef, useState} from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ListItem} from 'react-native-elements';
import RBSheet from 'react-native-raw-bottom-sheet';
import {ICCourts, ICSports, ICWellness} from '../../assets';
import {Bar, Gap} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../utils';
import SelectServices from '../WellnessCategoryPage/SelectServices';

const Category = [
  {
    id: 1,
    icon: <ICSports />,
    title: 'Basketball',
  },
  {
    id: 2,
    icon: <ICSports />,
    title: 'Soccer',
  },
  {
    id: 3,
    icon: <ICSports />,
    title: 'Tennis',
  },
  {
    id: 4,
    icon: <ICSports />,
    title: 'Swim',
  },
  {
    id: 5,
    icon: <ICSports />,
    title: 'Combat',
  },
];

const SportCategoryPage = () => {
  const navigation = useNavigation();

  const [visible, setVisible] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const refRBSheet = useRef();

  const handleBottomSheet = () => refRBSheet.current.open();

  const handleGroupClass = () => {
    navigation.navigate('Sports'), refRBSheet.current.close();
  };

  const handlePrivateTraining = () => {
    navigation.navigate('Trainer'), refRBSheet.current.close();
  };

  return (
    <View style={styles.page}>
      <Bar />
      <View>
        <HeaderCourse
          title="Select Sports"
          onPress={() => navigation.navigate('MainApp')}
        />
      </View>

      <Gap height={30} />
      {Category.map((item, i) => (
        <TouchableOpacity onPress={handleBottomSheet}>
          <RBSheet
            height={250}
            ref={refRBSheet}
            closeOnDragDown={true}
            closeOnPressMask={true}>
            <View>
              <View style={styles.containText}>
                <Text style={styles.text}>Select Services</Text>
              </View>
              <SelectServices
                icon={<ICCourts />}
                title="Rent Court"
                // onPress={() => {}}
              />
              <SelectServices
                icon={<ICWellness />}
                title="Group Class"
                onPress={handleGroupClass}
              />
              <SelectServices
                icon={<ICSports />}
                title="Private Training"
                onPress={handlePrivateTraining}
              />
            </View>
          </RBSheet>

          <ListItem key={item.id} style={styles.containlist}>
            <View style={styles.icon}>{item.icon}</View>
            <ListItem.Content>
              <ListItem.Title style={styles.title}>{item.title}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default SportCategoryPage;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white2},
  containText: {
    paddingHorizontal: 20,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {fontSize: 18, fontWeight: '700'},
  containlist: {paddingHorizontal: 20, marginBottom: 5},
  icon: {width: 40, height: 40, alignItems: 'center'},
  title: {fontFamily: fonts.Montserrat[700], fontSize: 14},
});
