import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ICSearch} from '../../assets';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {useNavigation} from '@react-navigation/native';
import {colors} from '../../utils';

const AvailableSchedulePage = () => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: colors.white2}}>
      <HeaderDetailClass
        onPress={() => navigation.navigate('MainApp')}
        onPressSearch={() => navigation.navigate('Search')}
        title="Available schedule"
        icon={<ICSearch />}
      />

      <Text>AvailableSchedulePage</Text>
    </View>
  );
};

export default AvailableSchedulePage;

const styles = StyleSheet.create({});
