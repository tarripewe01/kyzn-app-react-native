import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {ICArrowBack} from '../../assets';
import {Bar} from '../../components';
import HeaderCourse from '../../components/Atom/HeaderCourse';
import {colors, fonts} from '../../utils';
import {
  AllClasses,
  DanceClass,
  SwimmingClass,
  TennisClass,
  YogaClass,
} from './Classes';

const Tab = createMaterialTopTabNavigator();

const WellnessPage = () => {
  const navigation = useNavigation();
  return (
    <View style={{backgroundColor: colors.white2, flex: 1}}>
      <Bar />
      <View style={{marginTop: 20}}>
        <HeaderCourse
          icon={<ICArrowBack />}
          title="Wellness"
          onPress={() => navigation.navigate('MainApp')}
        />
      </View>

      <Tab.Navigator
        screenOptions={{
          tabBarLabelStyle: {fontSize: 12, fontFamily: fonts.Montserrat[700]},
          tabBarStyle: {width: '100%', paddingTop: 10},
          tabBarAllowFontScaling: true,
          tabBarLabelStyle: {textTransform: 'none'},
          tabBarIndicatorStyle: {backgroundColor: colors.black1},
          tabBarItemStyle: {
            width: 100,
            paddingHorizontal: -10,
            paddingVertical: -10,
          },
          tabBarScrollEnabled: true,
        }}>
        <Tab.Screen name="All" component={AllClasses} />
        <Tab.Screen name="Yoga" component={YogaClass} />
        <Tab.Screen name="Dance" component={DanceClass} />
        <Tab.Screen name="Tennis" component={TennisClass} />
        <Tab.Screen name="Swimming" component={SwimmingClass} />
      </Tab.Navigator>
    </View>
  );
};

export default WellnessPage;

const styles = StyleSheet.create({});
