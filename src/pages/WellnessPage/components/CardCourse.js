import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import {colors, fonts} from '../../../utils';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const imgXp = require('../../../assets/Images/image_xp.png');
const imgCoin = require('../../../assets/Images/image_coin.png');

const CardCourse = ({type, imgCourse, earn}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity onPress={() => navigation.navigate('Detail Class')}>
      <View style={styles.page}>
        <View style={styles.card}>
          <Image style={styles.imgCourse} source={imgCourse} />
          <View style={styles.content}>
            <Text numberOfLines={1} style={styles.title}>
              Yoga for Beginner
            </Text>
            <Text style={styles.price}>Rp 150.000</Text>
            {type === 'discount' ? (
              <Text style={styles.discount}>Rp 299.000</Text>
            ) : null}
            <View style={styles.reward(earn)}>
              <Text style={styles.status}>Earn</Text>
              <Image style={styles.imgXpCoin} source={imgXp} />
              <Text style={styles.xp_coin}>800</Text>
              <Image style={styles.imgXpCoin} source={imgCoin} />
              <Text style={styles.xp_coin}>800</Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CardCourse;

const styles = StyleSheet.create({
  page: {paddingHorizontal: 16},
  card: {
    width: wp('46%'),
    height: Platform.OS == 'android' ? hp('33%') : hp('27%'),
    marginRight: -25,
    borderWidth: 1,
    borderColor: colors.grey4,
    borderRadius: 5,
    // paddingBottom: 10
  },
  imgCourse: {width: 180, height: 100, resizeMode: 'cover'},
  content: {paddingHorizontal: 10, paddingTop: 12},
  title: {fontFamily: fonts.Montserrat[700], fontSize: 14},
  price: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    marginTop: 8,
  },
  discount: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 10,
    marginTop: 3,
    marginBottom: -18,
    color: colors.red2,
    textDecorationLine: 'line-through',
  },
  reward: earn => ({
    flexDirection: 'row',
    marginTop: earn === 'earn' ? 12 : 40,
    alignItems: 'center',
  }),
  status: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    color: colors.grey3,
    marginRight: 4,
  },
  imgXpCoin: {width: 20, height: 20, marginLeft: 3},
  xp_coin: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    marginLeft: 4,
  },
});
