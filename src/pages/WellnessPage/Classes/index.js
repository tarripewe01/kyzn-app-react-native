import AllClasses from './AllClasses';
import YogaClass from './YogaClass';
import DanceClass from './DanceClass';
import TennisClass from './TennisClass';
import SwimmingClass from './SwimmingClass';

export {AllClasses, YogaClass, DanceClass, TennisClass, SwimmingClass};
