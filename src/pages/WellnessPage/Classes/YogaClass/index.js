import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { ICArrowDwon } from '../../../../assets';
import { Gap } from '../../../../components';
import { colors, fonts } from '../../../../utils';
import CardCourse from '../../components/CardCourse';

const YogaClass = () => {
  return (
    <View style={styles.page}>
      <Gap height={24} />
      <View style={styles.contentText}>
        <Text style={styles.title}>Showing all 20 classes</Text>
        <TouchableOpacity style={styles.contentAction}>
          <Text style={styles.sort}>Sort by</Text>
          <ICArrowDwon />
        </TouchableOpacity>
      </View>
      <Gap height={20} />
      <ScrollView>
        <View style={{flexDirection: 'row'}}>
          <CardCourse
            type="discount"
            imgCourse={require('../../../../assets/Dummy/dummy_course1.png')}
            status="earn"
          />
          <CardCourse
            imgCourse={require('../../../../assets/Dummy/dummy_course3.png')}
            status=""
          />
        </View>
        <Gap height={12} />
        <View style={{flexDirection: 'row'}}>
          <CardCourse
            imgCourse={require('../../../../assets/Dummy/dummy_course3.png')}
            status=""
          />
          <CardCourse
            type="discount"
            imgCourse={require('../../../../assets/Dummy/dummy_course6.png')}
            status="earn"
          />
        </View>
        <Gap height={12} />
        <View style={{flexDirection: 'row'}}>
          <CardCourse
            imgCourse={require('../../../../assets/Dummy/dummy_course1.png')}
            status=""
          />
          <CardCourse
            imgCourse={require('../../../../assets/Dummy/dummy_course3.png')}
            status=""
          />
        </View>
        <Gap height={12} />
        <View style={{flexDirection: 'row'}}>
          <CardCourse
            imgCourse={require('../../../../assets/Dummy/dummy_course6.png')}
            status=""
          />
          <CardCourse
            imgCourse={require('../../../../assets/Dummy/dummy_course1.png')}
            status=""
          />
        </View>
        <Gap height={50} />
      </ScrollView>
    </View>
  );
};

export default YogaClass;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white2, flex: 1},
  contentText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 22,
    paddingRight: 18,
  },
  title: {
    fontFamily: fonts.Montserrat[500],
    fontSize: 12,
    color: colors.grey9,
  },
  contentAction: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  sort: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
    marginRight: 5,
  },
});
