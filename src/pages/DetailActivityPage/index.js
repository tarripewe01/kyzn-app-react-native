import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Bar, Gap} from '../../components';
import HeaderDetailClass from '../../components/Atom/HeaderDetailClass';
import {colors, fonts} from '../../utils';
import {Rating} from 'react-native-elements';

const DetailActivityPage = () => {
  const navigation = useNavigation();
  return (
    <>
      <Bar />

      <View style={styles.page}>
        <ScrollView>
          <View style={{marginTop: 20}}>
            <HeaderDetailClass
              title="Detail Activity"
              onPress={() => navigation.goBack('Activity')}
            />
          </View>
          <View style={styles.containInfo}>
            <View>
              <Text style={styles.paymentStatus}>Payment Status</Text>
              <Text style={styles.status}>SUCCESSFULL</Text>
            </View>
            <View>
              <Text style={styles.bookingStatus}>Booking Status</Text>
              <Text style={styles.booking}>COMPLETED</Text>
            </View>
          </View>

          <View style={styles.containContent}>
            <Text style={styles.textBooking}>Booking for</Text>
            <Text style={styles.location}>KYZN Gym Access</Text>
            <Text style={styles.date}>Tomorrow, 23 Sep 2020 | 18:00</Text>
          </View>
          <View style={styles.border}>
            <Text style={styles.textBooking2}>Excercise at</Text>
            <Text style={styles.location2}>Kyzn Studio - 3th Floor</Text>
            <Text style={styles.address}>
              Menara Kuningan - Jln Rasuna Sahid Level lorem ipsu...
            </Text>
          </View>
          <TouchableOpacity>
            <View style={styles.action}>
              <Text style={styles.textAction}>Show More</Text>
            </View>
          </TouchableOpacity>

          <View style={{paddingHorizontal: 20, marginTop: 32}}>
            <View style={styles.contain}>
              <Text style={styles.title}>Order ID</Text>
              <Text style={styles.title}>2387429</Text>
            </View>
            <View style={styles.contain}>
              <Text style={styles.title}>Date & Time</Text>
              <Text style={styles.title}> 23 Jan 2019 | 20:00</Text>
            </View>
            <View style={styles.contain}>
              <Text style={styles.title}>Payment Method</Text>
              <Text style={styles.title}> Bca Virtual Accounts</Text>
            </View>
            <View style={styles.contain}>
              <Text style={styles.title}>VA Number</Text>
              <Text style={styles.title}>08123234353453</Text>
            </View>
          </View>

          <Gap height={20} />
          <View style={styles.containRating}>
            <Text style={styles.textRating}>Rate This Class</Text>
            <Rating />
          </View>

          <Gap height={50} />

          <TouchableOpacity onPress={() => navigation.navigate('Detail Order')}>
            <View style={styles.button}>
              <Text style={styles.textButton}>SUBMIT</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </>
  );
};

export default DetailActivityPage;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white2,
  },
  containInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginTop: 20,
  },
  paymentStatus: {fontFamily: fonts.Montserrat[600], fontSize: 10},
  status: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 8,
    color: colors.green2,
  },
  bookingStatus: {fontFamily: fonts.Montserrat[600], fontSize: 10},
  booking: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 8,
    color: colors.green2,
  },
  containContent: {
    borderWidth: 1,
    borderColor: colors.grey4,
    paddingHorizontal: 16,
    paddingVertical: 12,
    width: Platform.OS === 'android' ? 370 : 360,
    justifyContent: 'center',
    marginTop: 20,
    marginHorizontal: 20,
  },
  textBooking: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey3,
  },
  location: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    marginTop: 10,
  },
  date: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey3,
    marginTop: 5,
  },
  border: {
    borderWidth: 1,
    borderColor: colors.grey4,
    paddingHorizontal: 16,
    paddingVertical: 12,
    width: Platform.OS === 'android' ? 370 : 360,
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  textBooking2: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey3,
  },
  location2: {
    fontFamily: fonts.Montserrat[700],
    fontSize: 12,
    marginTop: 10,
  },
  address: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
    color: colors.grey3,
    marginTop: 5,
  },
  action: {
    borderWidth: 1,
    borderColor: colors.grey4,
    paddingHorizontal: 16,
    paddingVertical: 12,
    width: Platform.OS === 'android' ? 370 : 360,
    justifyContent: 'center',
    marginHorizontal: 20,
    alignItems: 'center',
  },
  textAction: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 11,
  },
  containOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  contain: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },

  title: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 12,
  },
  containRating: {
    width: 350,
    height: 124,
    backgroundColor: colors.grey11,
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textRating: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 14,
    color: colors.grey12,
    marginBottom: 15,
  },
  button: {
    backgroundColor: colors.black2,
    width: 350,
    alignItems: 'center',
    height: 45,
    justifyContent: 'center',
    marginHorizontal: 20,
    borderRadius: 5,
    marginTop: Platform.OS === 'ios' ? 70 : 0,
  },
  textButton: {
    fontFamily: fonts.Montserrat[600],
    fontSize: 14,
    color: colors.white2,
  },
});
