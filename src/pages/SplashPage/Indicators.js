import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors} from '../../utils';

const Indicators = ({indicatorCount, currentSlideIndex}) => {
  if (!indicatorCount || typeof indicatorCount !== 'number') return null;

  let indicators = [];
  for (let i = 0; i < indicatorCount; i++) {
    indicators.push(i);
  }
  return indicators.map((indicators, index) => (
    <View
      key={indicators.toString()}
      style={[
        styles.indicator,
        index === currentSlideIndex ? styles.selected : styles.unselected,
      ]}
    />
  ));
};

export default Indicators;

const styles = StyleSheet.create({
  indicator: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 5,
  },
  selected: {
    backgroundColor: colors.black2,
  },
  unselected: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: colors.black2,
  },
});
