import React, {useEffect} from 'react';
import {Image, Platform, StyleSheet, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Bar from '../../components/Atom/Bar';
import { colors } from '../../utils';

const imageDecor = require('../../assets/Images/images_decor.png');
const imageLogo = require('../../assets/Images/images_logo.png');

const SplashPage = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Onboarding');
    }, 3000);
  }, [navigation]);

  return (
    <>
      <View style={styles.pages}>
        <Bar />
        <View>
          <Image source={imageLogo} style={styles.imageLogo} />
        </View>
      </View>
      <View style={styles.containImageDecor}>
        <Image source={imageDecor} style={styles.imageDecor} />
      </View>
    </>
  );
};

export default SplashPage;

const styles = StyleSheet.create({
  pages: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white2,
  },
  imageLogo: {
    width: Platform.OS === 'android' ? wp('48%') : wp('52%') ,
    height: hp('10%'),
    marginTop: 200,
  },

  containImageDecor: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  imageDecor: {
    width: wp('100%'),
    height: hp('54%'),
    marginTop: 80,
    backgroundColor: colors.white2,
  },
});
