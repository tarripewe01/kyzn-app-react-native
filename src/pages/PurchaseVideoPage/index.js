import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ICArrowBackWhite, ICTimer} from '../../assets';
import {colors, fonts} from '../../utils';
import {Gap} from '../../components';

const PurchaseVideoPage = () => {
  const navigation = useNavigation();
  return (
    <View style={{backgroundColor: colors.black1, flex: 1}}>
      <ScrollView>
        <StatusBar barStyle="light-content" backgroundColor={colors.black1} />
        <TouchableOpacity onPress={() => navigation.navigate('Workout Video')}>
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: 30,
              marginTop: 20,
            }}>
            <ICArrowBackWhite />
          </View>
        </TouchableOpacity>

        <View>
          <Image
            style={{width: '100%', height: 180}}
            source={require('../../assets/Dummy/dummy_video4.png')}
          />
        </View>

        <View style={{alignItems: 'center'}}>
          <Text
            style={{
              color: colors.white2,
              fontFamily: fonts.Montserrat[700],
              fontSize: 14,
              marginTop: 24,
            }}>
            Yoga for Beginner
          </Text>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <ICTimer />
            <Text
              style={{
                color: colors.grey8,
                fontFamily: fonts.Montserrat[600],
                fontSize: 12,
                marginLeft: 5,
              }}>
              15 Minutes
            </Text>
          </View>

          <View style={{paddingHorizontal: 20, marginTop: 16, width: 400}}>
            <Text
              style={{
                color: colors.white2,
                fontFamily: fonts.Montserrat[500],
                fontSize: 12,
                textAlign: 'center',
                lineHeight: 20,
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Text>
          </View>

          <View style={{marginTop: 170, alignItems: 'center'}}>
            <Text
              style={{
                color: colors.white2,
                fontFamily: fonts.Montserrat[700],
                fontSize: 14,
              }}>
              Rp 299.000
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate('Payment')}>
              <View
                style={{
                  backgroundColor: colors.white2,
                  width: 328,
                  height: 40,
                  borderRadius: 6,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 15,
                }}>
                <Text style={{fontFamily: fonts.Montserrat[700], fontSize: 14}}>
                  Purchase to Watch Video
                </Text>
              </View>
            </TouchableOpacity>
            <Gap height={20} />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default PurchaseVideoPage;

const styles = StyleSheet.create({});
