import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import {
  ICAccount,
  ICAccountActive,
  ICActivity,
  ICActivityActive,
  ICHome,
  ICHomeActive,
  ICInbox,
  ICInboxActive,
} from '../assets';
import {
  AccountPage,
  ActivityPage,
  AllActivityPage,
  AvailableSchedulePage,
  DetailActivityPage,
  DetailClassPage,
  DetailFitness,
  DetailInbox,
  DetailOrder,
  DetailSportsPage,
  DetailTrainerPage,
  FitnessCategoryPage,
  FitnessPage,
  HomePage,
  InboxPage,
  LoginPage,
  OnboardingPage,
  PaymentPage,
  PurchaseVideoPage,
  RegisterPage,
  SearchPage,
  SplashPage,
  SportCategoryPage,
  SportsPage,
  TrainerPage,
  WellnessCategoryPage,
  WellnessGroup,
  WellnessPage,
  WorkoutVideoPage,
  PaymentHistory,
  EditAccountPage,
  MyQRPage,
  MembershipPage,
  PremiumMembership,
  DetailLoyaltyPrograms,
  ZeniHistoryPage,
} from '../pages';
import {fonts} from '../utils';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarActiveTintColor: '#1035BB',
        tabBarLabelStyle: {fontSize: 10, fontFamily: fonts.Rubik[500]},
        tabBarStyle: {
          width: '100%',
          paddingVertical: 5,
          height: Platform.OS === 'android' ? 60 : 80,
        },
        tabBarAllowFontScaling: true,
        tabBarLabelStyle: {textTransform: 'none'},
      }}>
      <Tab.Screen
        name="Home"
        component={HomePage}
        options={{
          headerShown: false,
          tabBarIcon: ({color}) => (
            <View>{color == '#1035BB' ? <ICHomeActive /> : <ICHome />}</View>
          ),
        }}
      />
      <Tab.Screen
        name="Activity"
        component={ActivityPage}
        options={{
          headerShown: false,
          tabBarIcon: ({color}) => (
            <View>
              {color == '#1035BB' ? <ICActivityActive /> : <ICActivity />}
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Inbox"
        component={InboxPage}
        options={{
          headerShown: false,
          tabBarIcon: ({color}) => (
            <View>{color == '#1035BB' ? <ICInboxActive /> : <ICInbox />}</View>
          ),
        }}
      />
      <Tab.Screen
        name="Account"
        component={AccountPage}
        options={{
          headerShown: false,
          tabBarIcon: ({color}) => (
            <View>
              {color == '#1035BB' ? <ICAccountActive /> : <ICAccount />}
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="MainApp ">
      <Stack.Screen
        name="Splash"
        component={SplashPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Onboarding"
        component={OnboardingPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={LoginPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={RegisterPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Search"
        component={SearchPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Wellness"
        component={WellnessPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail Class"
        component={DetailClassPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Payment"
        component={PaymentPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Trainer"
        component={TrainerPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail Trainer"
        component={DetailTrainerPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Sports"
        component={SportsPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail Sport"
        component={DetailSportsPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Fitness"
        component={FitnessPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Workout Video"
        component={WorkoutVideoPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail Fitness"
        component={DetailFitness}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Purchase Video"
        component={PurchaseVideoPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Wellness Category"
        component={WellnessCategoryPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Wellness Group"
        component={WellnessGroup}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Sports Category"
        component={SportCategoryPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Fitness Category"
        component={FitnessCategoryPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Available Schedule"
        component={AvailableSchedulePage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail Activity"
        component={DetailActivityPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail Order"
        component={DetailOrder}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail Inbox"
        component={DetailInbox}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="All Activity"
        component={AllActivityPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Payment History"
        component={PaymentHistory}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Edit Account"
        component={EditAccountPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="My QR"
        component={MyQRPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Membership"
        component={MembershipPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Premium Membership"
        component={PremiumMembership}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Loyalty Program"
        component={DetailLoyaltyPrograms}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Zeni History"
        component={ZeniHistoryPage}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;

const styles = StyleSheet.create({});
