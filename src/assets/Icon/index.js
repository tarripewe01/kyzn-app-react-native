import ICEmail from './icon_email.svg';
import ICLock from './icon_lock.svg';
import ICGoogle from './icon_google.svg';
import ICArrowBack from './icon_arrowback.svg';
import ICHome from './icon_home.svg';
import ICHomeActive from './icon_home_active.svg';
import ICActivity from './icon_activity.svg';
import ICActivityActive from './icon_activity_active.svg';
import ICInbox from './icon_mail.svg';
import ICInboxActive from './icon_mail_active.svg';
import ICAccount from './icon_profile.svg';
import ICAccountActive from './icon_profile_active.svg';
import ICScan from './icon_scan.svg';
import ICSearch from './icon_search.svg';
import ICWellness from './icon_wellness.svg';
import ICFitness from './icon_fitness.svg';
import ICEducations from './icon_education.svg';
import ICSports from './icon_sports.svg';
import ICCourts from './icon_court.svg';
import ICNutritions from './icon_nutrition.svg';
import ICStar from './icon_star.svg';
import ICHistory from './icon_time_history.svg';
import ICBurger from './icon_burger.svg';
import ICProfile from './ic_profile.svg';
import ICPremium from './icon_premium.svg';
import ICArrowDwon from './icon_arrowdown.svg';
import ICEdit from './ic_edit.svg';
import ICLoyalty from './ic_loyalty.svg';
import ICQr from './icon_qrCode.svg';
import ICFaq from './icon_faq.svg';
import ICArrowNext from './icon_arrownext.svg';
import ICLockBlack from './ic_lockblack.svg';
import ICShopping from './icon_shopping.svg';
import ICPlayer from './icon_player.svg';
import ICGift from './icon_gift.svg';
import ICArrowNext2 from './icon_arrownextpurple.svg';
import ICShare from './icon_share.svg';
import ICSuccess from './icon_success.svg';
import ICWaiting from './icon_waiting.svg';
import ICCreditCard from './icon_creditcard.svg';
import ICVisa from './icon_visa.svg';
import ICDiscount from './icon_discount.svg';
import ICTimer from './ic_timer.svg';
import ICNotif from './icon_notif.svg';
import ICArrowBackWhite from './icon_arrowback_white.svg';
import ICYoga from './icon_yoga.svg';
import ICMeditation from './icon_meditation.svg';
import ICPilates from './icon_pilates.svg';
import ICRecovery from './icon_recovery.svg';
import ICMedicine from './icon_medicine.svg';
import ICBeautySpa from './icon_beautyspa.svg';
import ICHealing from './icon_healing.svg';
import ICFitness1 from './icon_categoryfitness1.svg';
import ICFitness2 from './icon_categoryfitness2.svg';
import ICFitness3 from './icon_categoryfitness3.svg';
import ICFitness4 from './icon_categoryfitness4.svg';
import ICFitness5 from './icon_categoryfitness5.svg';
import ICBCA from './icon_BCA.svg';
import ICEditBlue from './icon_edit.svg';
import ICCamera from './icon_camera.svg';
import ICClose from './icon_close.svg';
import ICQrWhite from './icon_qrCodeWhite.svg';
import ICCheck from './icon_check.svg';
import ICLoyaltyProgram from './icon_loyalty.svg';
import ICCheckBlack from './check.svg';

export {
  ICEmail,
  ICLock,
  ICGoogle,
  ICArrowBack,
  ICHome,
  ICHomeActive,
  ICActivity,
  ICActivityActive,
  ICInbox,
  ICInboxActive,
  ICAccount,
  ICAccountActive,
  ICScan,
  ICSearch,
  ICWellness,
  ICFitness,
  ICEducations,
  ICSports,
  ICCourts,
  ICNutritions,
  ICStar,
  ICHistory,
  ICBurger,
  ICProfile,
  ICPremium,
  ICArrowDwon,
  ICEdit,
  ICLoyalty,
  ICQr,
  ICFaq,
  ICArrowNext,
  ICLockBlack,
  ICShopping,
  ICPlayer,
  ICGift,
  ICArrowNext2,
  ICShare,
  ICSuccess,
  ICWaiting,
  ICCreditCard,
  ICVisa,
  ICDiscount,
  ICTimer,
  ICNotif,
  ICArrowBackWhite,
  ICYoga,
  ICMeditation,
  ICPilates,
  ICRecovery,
  ICMedicine,
  ICBeautySpa,
  ICHealing,
  ICFitness1,
  ICFitness2,
  ICFitness3,
  ICFitness4,
  ICFitness5,
  ICBCA,
  ICEditBlue,
  ICCamera,
  ICClose,
  ICQrWhite,
  ICCheck,
  ICLoyaltyProgram,
  ICCheckBlack,
};
